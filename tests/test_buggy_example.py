from graph_classes import Graph, Bag
from tree_diet import tree_diet

def test_buggy():

    
    G = Graph()

    for i in range(9):
        G.add_vertex(i)

    G.add_edge(1,2)
    G.add_edge(2,3)

    G.add_edge(2,5)

    R = Bag([])

    B1 = Bag([7,8])
    B2 = Bag([6,7,8])
    B3 = Bag([4,6,7,8])
    B4 = Bag([3,4,6,7,8])
    B5 = Bag([0,3,4,6,7,8])
    B6 = Bag([0,3,4,5,6])
    B7 = Bag([0,1,2,3,5])

    R.add_child(B1)
    B1.add_child(B2)
    B2.add_child(B3)
    B3.add_child(B4)
    B4.add_child(B5)
    B5.add_child(B6)
    B6.add_child(B7)

    must_have_edges = [(1,2),(2,3)]

    num_real, list_edges, _ = tree_diet(R, G.adj, 2, must_have_edges)

    for e in must_have_edges:
        assert(e in list_edges)
   
    assert(len(list_edges)==2) 

if __name__=='__main__':
    test_buggy()

import matplotlib.pylab as plt
import networkx as nx
from graph_cpp_routines import print_graph, components
from graph_pylib import Graph, plot_graph, all_min_seps
import random


def test_print_graph():

    g = Graph(40)

    for i in range(40):
        for j in range(40):
            if random.random() < 0.2:
                g.add_edge(i, j)

    print_graph(g.adj, 40)


def test_components():

    g = Graph(40)

    for i in range(40):
        for j in range(40):
            if random.random() < 0.2:
                g.add_edge(i, j)

    comp, ngbh = components(set([1, 2, 3]), g.adj, 40)

    g2 = Graph(10)

    for i in range(9):
        g2.add_edge(i, i+1)

    comp, ngbh = components(set([4, 5, 6]), g2.adj, 10)

    print("comp", comp)
    print("ngbh", ngbh)
    assert(len(comp.keys()) == 2)
    assert(comp[0] == {0, 1, 2, 3})
    assert(comp[1] == {7, 8, 9})
    assert(ngbh[0] == {4})
    assert(ngbh[1] == {6})

    g3 = Graph(21)

    for i in range(10):
        for j in range(10):
            g3.add_edge(i, j)

    for i in range(10, 20, 1):
        for j in range(10, 20, 1):
            g3.add_edge(i, j)

    g3.add_edge(8, 20)
    g3.add_edge(14, 20)

    comp, ngbh = components({20}, g3.adj, 21)

    assert(len(comp.keys()) == 2)
    assert(comp[0] == set(range(10)))
    assert(comp[1] == set(range(10, 20, 1)))
    assert(ngbh[0] == {20})
    assert(ngbh[1] == {20})

    h = 3
    l = 4

    grid = nx.grid_graph(dim=[h, l])

    g_grid = Graph(h*l)

    for (u, v), (s, t) in grid.edges:
        print(u*h+v, h*s+t)
        g_grid.add_edge(u*h+v, h*s+t)

    comp, ngbh = components({1, 3, 4, 5, 7}, g_grid.adj, 12)

    print("comp", comp)
    print("ngbh", ngbh)
    assert(len(comp.keys()) == 3)
    assert(comp[0] == {0})
    assert(comp[1] == {2})
    assert(comp[2] == {6, 8, 9, 10, 11})
    assert(ngbh[0] == {1, 3})
    assert(ngbh[1] == {1, 5})
    assert(ngbh[2] == {3, 5, 7})


def test_all_min_seps():

    g2 = Graph(10)

    for i in range(9):
        g2.add_edge(i, i+1)

    min_seps = all_min_seps(g2)

    assert(len(min_seps) == 8)

    h = 3
    l = 3

    grid = nx.grid_graph(dim=[h, l])

    g_grid = Graph(h*l)

    for (u, v), (s, t) in grid.edges:
        g_grid.add_edge(u*h+v, h*s+t)

    min_seps = all_min_seps(g_grid)

    assert(len(min_seps) == 20)


if __name__ == '__main__':
    # test_print_graph()
    # test_components()
    test_all_min_seps()

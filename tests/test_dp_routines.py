from graph_pylib import Graph, TreeDecomposition
from optimal_coloring_implem import compatible_colorings, produce_entries
from optimal_coloring_implem import optimal_coloring, hashabledict
from optimal_coloring_implem import compute_unrealizable


def test_compatible_colorings():

    TD = TreeDecomposition()

    TD.bags[0] = [0, 1, 2]
    TD.bags[1] = [1, 2, 3]

    TD.adj[0] = set([1])

    cc = compatible_colorings(0, {}, TD)

    for l in cc:
        print(l)


def test_produce_entries():

    TD = TreeDecomposition()

    TD.bags[0] = [0, 1, 2]
    TD.bags[1] = [1, 2, 3]

    TD.adj[0] = set([1])
    TD.adj[1] = set([])

    entries, cc_table = produce_entries(TD)

    for e in entries:
        print(e)


def test_optimal_solve():

    G = Graph(4)

    G.add_edge(0, 1)
    G.add_edge(0, 2)
    G.add_edge(1, 2)
    G.add_edge(3, 1)
    G.add_edge(3, 2)
    G.add_edge(1, 2)

    TD = TreeDecomposition()

    TD.bags[0] = [0, 1, 2]
    TD.bags[1] = [1, 2, 3]

    TD.adj[0] = set([1])
    TD.adj[1] = set([])

    c, trace = optimal_coloring(TD, G, 1)

    for key, val in c.items():
        print(key, val)

    print("trace")
    for key, val in trace.items():
        print(key, val)

    assert(c[0, hashabledict()] == 3)


def test_compute_unrealizable():

    G = Graph(3)

    G.add_edge(0, 1)
    G.add_edge(0, 2)
    G.add_edge(1, 2)

    TD = TreeDecomposition()

    TD.bags[0] = [0, 1, 2]

    d = hashabledict()

    d[0] = 'g'
    d[1] = 'g'
    d[2] = 'o'

    f = {}
    f[0] = d

    unr = compute_unrealizable(f, G, TD)

    assert(len(unr) == 2)

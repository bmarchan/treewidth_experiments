import networkx as nx
import random
from graph_pylib import Graph, TreeDecomposition, plot_graph


def test_is_valid():

    g = Graph(10)

    for i in range(10):
        for j in range(10):
            g.add_edge(i, j)

    TD = TreeDecomposition()

    TD.adj[0] = []
    TD.bags[0] = set(range(10))

    assert(TD.is_valid(g))

    print("second graph")
    g2 = Graph(20)

    for i in range(19):
        g2.add_edge(i, i+1)

    TD = TreeDecomposition()

    for i in range(20):
        TD.adj[i] = set([])

    for i in range(19):
        TD.bags[i] = set([i, i+1])
        if i < 18:
            TD.adj[i].add(i+1)
            TD.adj[i+1].add(i)

    print("bags ", TD.bags)
    print("adj ", TD.adj)

    assert(TD.is_valid(g2))

    TD.pick_root(5)
    assert(TD.is_valid(g2))


def test_build_from_elim_order():

    g = Graph(20)

    for i in range(19):
        g.add_edge(i, i+1)

    TD = TreeDecomposition()

    TD.build_from_elim_order(g, list(range(20)))

    print("bags ", TD.bags)
    print("TD adj ", TD.adj)

    assert(TD.is_valid(g))

    print("NEW GRAPH")

    h = 3
    l = 10

    grid = nx.grid_graph(dim=[h, l])

    g_grid = Graph(h*l)

    for (u, v), (s, t) in grid.edges:
        g_grid.add_edge(u*h+v, h*s+t)

#    plot_graph(g_grid, show=True)

    TD.build_from_elim_order(g_grid, list(range(h*l)))

    print("bags ", TD.bags)
    print("TD adj ", TD.adj)

    assert(TD.is_valid(g_grid))

    n = 6
    g = Graph(n)

    random.seed(2)

    for i in range(n):
        for j in range(n):
            if random.random() < 0.5:
                g.add_edge(i, j)

#    plot_graph(g, show=True)

    TD = TreeDecomposition()

    TD.build_from_elim_order(g, list(range(n)))

    print("bags ", TD.bags)
    print("adj ", TD.adj)

    assert(TD.is_valid(g))


def test_build_from_nx_fill_in():

    g = Graph(4)

    g.add_edge(0, 1)
    g.add_edge(0, 2)
    g.add_edge(1, 3)
    g.add_edge(0, 3)

    TD = TreeDecomposition()

    TD.from_nx_min_fill_in(g)

    assert(TD.is_valid(g))


def test_pick_root():

    g = Graph(4)

    g.add_edge(0, 1)
    g.add_edge(0, 2)
    g.add_edge(1, 3)
    g.add_edge(0, 3)

    TD = TreeDecomposition()

    TD.from_nx_min_fill_in(g)

    TD.pick_root()

    assert(TD.is_valid(g))

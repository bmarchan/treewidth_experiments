from graph_pylib import Graph, plot_graph


def test_graph_basic():

    g = Graph(4)

    g.add_edge(0, 1)
    g.add_edge(2, 1)

    g.add_edge(45, 54)
    g.add_edge(1, 1)

    print(g.adj)
#    assert(False)


def test_graph_drawing(show=False):

    g = Graph(4)

    for i in range(4):
        for j in range(4):
            g.add_edge(i, j)

    plot_graph(g, show=show)


def test_to_nx():

    g = Graph(4)

    g.add_edge(0, 1)
    g.add_edge(0, 2)
    g.add_edge(1, 3)
    g.add_edge(0, 3)

    nxg = g.to_nx()

    assert(nxg.order())


if __name__ == "__main__":
    test_graph_drawing(show=True)

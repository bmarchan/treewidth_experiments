import pytest
from dbn_module import compute_dbn
from graph_cpp_routines import MCF

def test_MCF_call():

    mcf = MCF([], 10)

    assert(len(mcf)==0)

def test_dbn_small():

    n_vertices = 10

    arc_list = [(0,9), (1, 8), (3, 7)]
#
    dbn_str = compute_dbn(arc_list, n_vertices)

    print(dbn_str)

    assert(dbn_str[0]=='((.(...)))')

def test_dbn2():

    n_vertices = 12

    arc_list = [(0,9), (1, 8), (3, 7), (5, 11), (6, 10)]
#
    dbn_str = compute_dbn(arc_list, n_vertices)

    print(dbn_str)

    assert(dbn_str[0]=='((.(.[[)))]]')

def test_dbn3():

    n_vertices = 13

    arc_list = [(0,9), (1, 8), (3, 7), (5, 12), (6, 11), (4, 10)]
#
    dbn_str = compute_dbn(arc_list, n_vertices)

    print(dbn_str)

    assert(dbn_str[0]=='((.(<[[)))>]]')

if __name__=='__main__':
#    test_dbn_small()
    test_dbn3()
    pass

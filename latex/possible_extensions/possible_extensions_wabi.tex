\documentclass{article}

\usepackage{natbib}
\usepackage{bibentry}
\usepackage{graphicx}

\newcommand{\fancybibentry}[1]{\item \cite{#1} \textbf{\bibentry{#1}}~\\~\\}

\title{Possible extensions to WABI paper for submission to 
Algorithms for Molecular Biology by November 15}

\begin{document}
\maketitle
\bibliographystyle{unsrt}
\nobibliography{extensions_wabi_refs}

\tableofcontents

\section{New references, more bibliographical context}

\begin{itemize}

\fancybibentry{song2005tree}

They formulate structure-sequence alignment of a structured pseudo-knotted
RNA with a plain sequence as instances of 
maximum-weight subgraph isomorphism. From the RNA structure,
a ``conformational graph'' $H$ is constructed. The vertices of $H$
are basic structural elements of the structure (the faces of the planar graph
associated to the secondary structure ?) whose exact definitions are not clear.
To each of these vertices is associated a mini covariance model. This 
mini-covariance model is used to look at some ``hits'' for the corresponding
structural sub-unit in the target sequence. The maximum number of hits
``per stem'' (is it per vertex ? Or does a stem encompass several vertices ?)
is called $k$. In any case, these hits help form the ``image graph'' $G$.
The purpose then is to find a maximum weight embedding of $H$ into $G$.
They suggest using dynamic programming on tree decompositions of $H$.
The overall algorithm complexity is $O(k^{tw}|H|^2)$. $k$ being the maximum
number of images for a vertex of $H$, it is linked to the size of the 
``image graph'' $G$. 

\fancybibentry{huang2008fast}

Follow-up of \cite{song2005tree} above. Same formulation of RNA structure-sequence
alignment problem as subgraph isomorphism, for a graph formed from
``structural subunits'' and another from the ``images'' obtained in the
plain sequence for each of these subunits. Interestingly, they give a heuristic
for the treewidth of RNA graphs. It consists in starting from a tree decomposition
of a maximal secondary structure (pseudoknot-free) and then ``greedily'' adding
vertices in bags to take the other edges into account.

\item Some heuristic coming from ``bayesian networks'' to reduce tree-width
 ? I thought I had seen something like that but cannot find it. Very
different from our work anyway.

\end{itemize}

\section{Mentioning and elaborating on the ``backbone edge removal'' problem}

\subsection{Introduction}

\paragraph{Introduction} In the experiments, we have put a very large weight
on the removal by the tree-diet of back-bone edges in RNA graphs. Yet,
we noticed that some tree diets still made some of back-bone edges
unrealizable, in spite of this large weight. 

A practical consequence is that LiCoRNA crashes (segmentation fault) if
given such a back-bone breaking tree decomposition as input. 

Whether LiCoRNA could in theory 
be adapted to allow for a small number of such "back-bone" breaks is unclear
to me. 

Given the central role played by LiCoRNA in our experiments, and in the
tree-diet project in general, it is important to try and understand a few things:
\begin{itemize}
\item How can such ``inevitable backbone breaks'' occur ?
\item Is it common ?
\item Can we pre-process a tree decomposition to prevent it ?
\item Can we detect beforehand that this would be the case ?
\end{itemize}

\paragraph{Results of this section} In this section, we first give
a detailed example of a tree-diet in which such an inevitable breakage arises.
From this example, we define a notion of ``obstacle'' whose presence
in a tree decomposition implies that a back-bone edge will necessarily be broken
during a diet. The converse (is the backbone always conserved given an
obstacle-free tree decomposition ?) is not true, and we give a
simple counter-example. On the positive-side, this obstacle is detectable
beforehand in polynomial-time. We use an implementation of this detection
to empirically show that, indeed, inevitable back-bone breaking seems 
very common. As for pre-processing, we introduce a re-writing rule that,
under specific conditions, removes an obstacle from a tree decomposition 
at the price of removing non-back-bone edges.

\subsection{An example}

See a hand-drawn example extracted from an RNA instance (RF01689 - 4FRG),
wherever latex has decided to put it.

\begin{center}
\begin{figure}
\includegraphics[width=\textwidth]{IMG3615.jpeg}
\caption{Example of a situation (RF01689 - 4FRG) 
which forces at least one backbone edge to break in case of a diet. 
The picture represents a bag of a tree
decomposition, along with the backbone edges involving the vertices of the bag.
The bag has four neighboring bags, defining 4 sub-trees (represented
by the ``neighboring regions'' on the picture). The backbone-neighbors
of the vertices of the bags have been placed in the regions where they are
represented, which the place where the corresponding backbone edges are 
realized. \textbf{For each vertex in this bag, the two backbone
edges involving the vertex are realized in different subtrees. Therefore
removing the vertex from the bag implies breaking at least one of them.}}
\end{figure}
\end{center}

\subsection{Obstacle}

From the example of the last section, we define a simple notion
of ``obstacle'' to a tree-diet given a set of ``important edges'' that
one wishes to keep.

\paragraph{Definition ``locked vertex''} A vertex is said to be locked
in a bag if it is involved in at least 2 important edges that
are realized on different sides of an edge of the bag (an edge of the tree decomposition).
It is said to be ``locked'' because removing it from the bag implies
breaking at least one ``important edge''.  

\paragraph{Definition ``problematic bag'' or ``obstacle''} A bag is said
to be problematic with respect to a target width $w$ if it possesses
strictly more
than $w+1$ locked vertices. 

\paragraph{Absence of obstacle yet backbone broken} If the presence
of an obstacle implies the breaking of the backbone, it is possible
to have only ``non-problematic'' bags and yet be in a situation where
a backbone edge will necessarily be broken in case of a diet. See an informal example
below:
\begin{itemize}
\item Consider a bag $A=\{1,2,3,4,5,6\}$ in which, say, $1,2,3,4$ are ``locked''
in the sense defined above.
\item $5,6$ is an important edge, the only one in which $5$ and $6$ are involved.
Since they are only involved in one important edge, $5$ and $6$ are not ``locked''
in the sense defined above.
\item If in addition $A$ is the only bag where $5,6$ is realized, then 
they cannot be removed without breaking this edge.
\end{itemize}

\subsection{Prevalance}

The detection of this obstacle in tree decompositions has been implemented
in Python. Given a tree decomposition and a target width, the program
returns either ``A diet preserving important edges is definitely impossible''
or ``A diet preserving important edges is maybe possible''.

On RF01689, 4 tree decompositions out of 5 (5 heuristics are launched
to compute tree decompositions) present such an obtacle. The fifth one
has width 17 and is probably impracticle for a tree diet.

\subsection{Possible workaround, using Infrared ?}

Sebastian and an intern (Justine) have worked on implementing
structure-sequence alignment using Infrared. Contrary to LiCoRNA, which,
in its current state, simply crashes when a tree decomposition that does
not preserve the backbone is given as input, Infrared could still return
something.

Indeed, Infrared formulates the problem in terms of a Constraint Network.
Each edge of the graph introduces new constraints or cost functions involving
the variables associated to the two vertices. Therefore, when a backbone
edge is missing, then some constraints are removed from the network, but
it can still be solved, and ``something'' is returned. Could that something
be used to get a lower bound for the global optimal cost of aligning the 
full model ? In a similar fashion as what we do in the appendix of the full
version of our article ?
If we can, then the resulting bound will certainly not be as strong as if we used a reduced
model that preserved the backbone, but the overal ``hierarchical filtering''
strategy could still be done.

\section{Benchmark of tree-diet itself}

In spite of the scary $O(12^{tw})$, the tree-diet algorithm actually
runs reasonably fast, and is far from being the most expensive
part of our numerical pipelines. That is nice !

\section{Showing how a lot of RNA graphs have quite low treewidth, but not low enough for LiCoRNA.}

See screenshot of my terminal wherever latex has decided to put it.

\begin{center}
\begin{figure}
\includegraphics[width=\textwidth]{treewidthsummary.png}
\caption{Screenshot of my terminal, showing a summary of treewidth values
for all RNA structures extracted from the PDB database ($\sim 5000$ RNA
structures), taking only \textbf{canonical} base pairs into account.
For each structure, a variety of treewidth heuristics have been launched,
and the best result has been kept. The table shows that
1930 structures have treewidth 2, 777 have treewidth 3, etc. 
\textbf{A substantial number of structures have treewidth values between
5 and 8-9, a range in which tree-diet is relevant (tree diet can
be easily run, but not LicoRNA)}}
\end{figure}
\end{center}


\end{document}

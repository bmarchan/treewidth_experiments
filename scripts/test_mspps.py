import networkx as nx
from graph_pylib import Graph, all_min_seps, sample_mspps, plot_graph 

import matplotlib

cmap = matplotlib.cm.get_cmap('Spectral')


h = 3
l = 4

grid = nx.grid_graph(dim=[h,l])

g_grid = Graph(h*l)


for (u,v),(s,t) in grid.edges:
    g_grid.add_edge(u*h+v,h*s+t)

seps = all_min_seps(g_grid)

print("type ", type(seps))

mspps = sample_mspps(seps, g_grid)

print("mspps", mspps)

node_color = []
node_pos = []



for sep in mspps:

    node_color = []
    for i in range(h*l):
        print(i, sep, i in sep)
        node_pos.append((i/h,i%h))
        if i in sep:
            node_color.append('b')
        else:
            node_color.append('r')

    print(sep)
    plot_graph(g_grid, node_color=node_color, pos=node_pos, show=True) 

#for u in range(g_grid.n):
#    found = False
#    for k, sep in enumerate(mspps):
#        if u in sep:
#            found = True
#            color = float(k)/float(len(mspps))
#            break
#
#    if found:
#        node_color.append(cmap(color))
#    else:
#        node_color.append('b')
#
#plot_graph(g_grid, node_color=node_color, show=True)

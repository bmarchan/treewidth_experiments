import random
from graph_pylib import Graph, plot_tree_dec, TreeDecomposition

n = 6
g = Graph(n)

random.seed(2)

for i in range(n):
    for j in range(n):
        if random.random() < 0.5:
            g.add_edge(i,j)

TD = TreeDecomposition()

TD.build_from_elim_order(g, list(range(n)))

plot_tree_dec(TD)

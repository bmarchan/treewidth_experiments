import networkx as nx
from graph_cpp_routines import print_graph, components
from graph_pylib import Graph, plot_graph, all_min_seps


h = 3
l = 3

grid = nx.grid_graph(dim=[h,l])

g_grid = Graph(h*l)

for (u,v),(s,t) in grid.edges:
    g_grid.add_edge(u*h+v,h*s+t)

node_pos = []
for (u,v) in grid.nodes:
    node_pos.append((u,v))

for u in range(h*l):
    comp, ngbh = components(g_grid.adj[u].union({u}), g_grid.adj, h*l)
    print("u comp ngbh ", u, comp, ngbh) 

min_seps = all_min_seps(g_grid)

cnt = 0

for sep in min_seps:
    node_color = []
    for i in range(h*l):
        print(i, sep, i in sep)
        if i in sep:
            node_color.append('b')
        else:
            node_color.append('r')

    print(sep)
    cnt += 1
    plot_graph(g_grid, pos = node_pos, node_size=800, 
               node_color=node_color, show=True, savename="sep"+str(cnt)) 

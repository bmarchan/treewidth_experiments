import matplotlib.pylab as plt
import numpy as np
from rna_graphs_gen import random_std_triple_helix
from graph_pylib import plot_graph
import networkx as nx

n = 40

helix, pivots = random_std_triple_helix(n, 
                                        edge_proba=0.75,
                                        seed=457)

nx_g = nx.Graph()

node_color = []

positions = {}

for i in range(helix.n):
    nx_g.add_node(i)
    if i in pivots:
        node_color.append(1)
    else:
        node_color.append(0)
    if i <= pivots[1]:
        positions[i] = np.array([0,i/float(pivots[1])])
    elif i <= pivots[2]:
        p2 = pivots[2]
        p1 = pivots[1]
        positions[i] = np.array([1,1-(i-pivots[1])/float(p2-p1)])
    else:
        p2 = pivots[2]
        p3 = pivots[3]
        positions[i] = np.array([2,(i-p2)/float(p3-p2)])

edge_color = []

for i in range(helix.n):
    for j in helix.adj[i]:
        if i < j:
            nx_g.add_edge(i,j)
            if j==(i+1):
                edge_color.append('black')
                continue
            if i <= pivots[1]: 
                edge_color.append('r')
            elif i <= pivots[2]:
                edge_color.append('g')

#print("positions ", nx.circular_layout(nx_g))

nx.draw_networkx(nx_g, pos=positions, 
                 node_color=node_color,
                 edge_color=edge_color)

plt.savefig("triple_helix.eps")
plt.show()

#from graph_cpp_routines import bag
from graph_pylib import recurse_print, nicify
from graph_pylib import py_bag as bag

R = bag([])

B1 = bag([0,1,2])

B2 = bag([2,3,4])

B3 = bag([6,7])

R.add_child(B1)
B1.add_child(B2)
B1.add_child(B3)

recurse_print(R, 0)

nicify(R)
#R.make_nice(B1)
#B1.make_nice(B2)
#B1.make_nice(B3)
#
print("-------------------------------------------------------------------")

recurse_print(R, 0)

R = bag([])

B1 = bag([1,2])

B2 = bag([3,4])

B3 = bag([5,6])

R.add_child(B1)
R.add_child(B2)
R.add_child(B3)

recurse_print(R,0)
R.dupli_nice()
print("------------dupli nice -------------------")
recurse_print(R,0)


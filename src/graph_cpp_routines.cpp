#include <pybind11/stl.h>
#include <pybind11/pybind11.h>
#include <map>
#include <math.h>
#include <utility>
#include <functional>
#include <list>
#include <set>
#include <iostream>
#include <string>

#define GREEN 1
#define ORANGE 2
#define RED 3
#define DEBUG 1

using namespace std;
namespace py = pybind11;

// typedefs for convenience
typedef map<int, set<int>> adjacency;
typedef set<int> subset;
typedef map<int, set<int>> set_of_subsets;

void print_spaces(int depth)
{
    for (int i = 0; i < depth; i++)
    {
        cout << "    "; 
    }
}

void print_graph(adjacency adj, int size)
{
    for (int i = 0; i < size; i++)
    {
        for (auto j : adj[i])
        {
            cout << i << " " << j << endl;
        }
    }
}

pair< set_of_subsets, set_of_subsets > components(subset X, adjacency adj, int size)
{
    pair< set_of_subsets, set_of_subsets >  result_pair;

    set_of_subsets components;
    set_of_subsets ngbh;

    // visited or not
    map<int, bool> visited;

    for (int i = 0; i < size; i++)
    {
        if (X.find(i) != X.end())
        {
            visited[i] = true;
        }
        else
        {
            visited[i] = false;
        }
    }

    // main loop
    bool not_all_visited = true;

    int color = 0;
    components[color] = {};
    ngbh[color] = {};

    while (not_all_visited)
    {
        // to be updated at end of loop
        not_all_visited = false;

        // find seed
        int seed;

        for (int i = 0; i < size; i++)
        {
            if (!visited[i])
            {
                seed = i;
                break;
            }
        }

        components[color].insert(seed);
        visited[seed] = true;
        

        // build frontier (initialized to adj[seed])
        list<int> frontier;    

        for (auto j : adj[seed]) 
        {
            if (!visited[j]) frontier.push_back(j);
            else
            {
                // on the fly component neihborhood construction
                if (X.find(j) != X.end()) ngbh[color].insert(j); 
            }
        }

        // propagate frontier
        while (!frontier.empty())
        {

            int i = frontier.back();
            frontier.pop_back();

            components[color].insert(i);
            visited[i] = true;
 
            for (auto j : adj[i])
            {
                if (!visited[j]) frontier.push_back(j);
                else
                {
                    // on the fly component neihborhood construction
                    if (X.find(j) != X.end()) ngbh[color].insert(j);
                }
            }
        }

        color += 1;
        
        for (int i =0; i < size; i++)
        {
            if (!visited[i]) 
            {        
                not_all_visited = true;
                break;
            }
        }

        if (not_all_visited)
        {
            components[color] = {};
            ngbh[color] = {};
        }
    }

    result_pair.first = components;
    result_pair.second = ngbh;

    return result_pair;
}

list<pair<int,int>> backtrack(int i,
                              int j, 
                              int** c, 
                              map<int,int> arc_lut,
                              int string_length)
{
    list<pair<int,int>> empty_list;


    if (i >= j)
    {
        return empty_list;
    } 
    else
    {
        if (arc_lut[i] >= 0)
        {
            if (arc_lut[i] <= j)
            {
                
                int k = arc_lut[i];

                int candidate_value;
                if (k+1 < string_length)
                {
                    candidate_value = 1+c[i+1][k-1]+c[k+1][j];
                }
                else
                {
                    candidate_value = 1+c[i+1][k-1];
                }

                if (c[i][j]==candidate_value)
                {
                    list<pair<int,int>> result_list, l1, l2;

                    pair<int,int> result_pair; 
                    result_pair.first = i;
                    result_pair.second = k;

                    result_list.push_back(result_pair);

                    l1 = backtrack(i+1, k-1, c, arc_lut, string_length);
                    if (k+1 < string_length)
                    {
                        l2 = backtrack(k+1, j, c, arc_lut, string_length);
                    }
                    
                    result_list.insert(result_list.end(), l1.begin(), l1.end());
                    result_list.insert(result_list.end(), l2.begin(), l2.end());

                    return result_list;

                }
                else
                {
                    return backtrack(i+1, j, c, arc_lut, string_length);
                }
            }
            else
            {
                return backtrack(i+1, j, c, arc_lut, string_length);
            }
        }
        else
        {
            return backtrack(i+1, j, c, arc_lut, string_length);
        }
    }

    cout << "should not come to here" << endl;
    return empty_list; 
}

list<pair<int,int>> MCF(list<pair<int,int>> arcs, int string_length)
{
    /* ----------------------------> j
    // |\                          * -> overal solution
    // |      \                     
    // |             \               
    // |                    \       
    // |  [don't care]             \
    / \/ i
    */

    // converting list of arc to O(1) look-up table:

    map<int, int> arc_lut;

    // default to un-paired
    for (int i = 0; i < string_length; i++)
    {
            arc_lut[i] = -1;
    }

    // looping over arcs
    for (auto const & arc : arcs)
    {
        arc_lut[arc.first] = arc.second;
    }

    // dynamically allocating table, starting with pointers to lines, i.e
    // first index, i.e right position of interval
    
    int** c = new int*[string_length];

    // allocating second index, j  
    for (int i = 0; i < string_length; i++)
    {
        c[i] = new int[string_length];
    }

    // diagonal initialization
    for (int k = 0; k < string_length; k++)  
    {
        c[k][k] = 0;
    }
    
    // just below diagonal init to 0
    for (int i = 1; i < string_length; i++)
    {
        c[i][i-1] = 0;    
    }


    for (int diff = 1; diff < string_length; diff++)
    {
        for (int i = 0; i < string_length - diff; i++)
        {
            int j = i + diff;

            pair<int, int> id_pair;

            if (arc_lut[i] >= 0)
            {
                if (arc_lut[i] <= j)
                {
                    int k = arc_lut[i];
                    if (k+1 < string_length)
                    {
                        c[i][j] = max(1+c[i+1][k-1]+c[k+1][j], c[i+1][j]);
                    }
                    else
                    {
                        c[i][j] = max(1+c[i+1][k-1], c[i+1][j]);
                    }
                }
                else
                {
                    c[i][j] = c[i+1][j];
                }
            }
            else
            {
                c[i][j] = c[i+1][j];
            }

        }
    }

    list<pair<int,int>> result_list; 


    result_list = backtrack(0, string_length - 1, c, arc_lut, string_length); 

    // deallocate memory using delete[] operator
    for (int i = 0; i < string_length; i++)
        delete[] c[i];
 
    delete[] c;

    return result_list;
}

PYBIND11_MODULE(graph_cpp_routines, m) {
    m.doc() = "cpp routines for graphs"; // optional module docstring

    m.def("components", 
           &components, 
           "A function which computes the components associated to a subset");
    
    m.def("print_graph", 
           &print_graph, 
           "prints graph");
    
    m.def("MCF", 
           &MCF, 
           "function computing maximum conflict-free subset of a set of arcs");
}

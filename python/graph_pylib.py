from itertools import combinations
import matplotlib.pylab as plt
import random
from graph_cpp_routines import components
from tree_diet_cpp import bag
from networkx.algorithms.approximation.treewidth import treewidth_min_fill_in

import networkx as nx  # plotting utilities
import numpy as np


# IMPORTANT CLASSES

class Graph:

    def __init__(self, n_vertices, index_shift=0):

        self.adj = {}
        self.n = n_vertices

        for i in range(n_vertices):
            self.adj[i+index_shift] = set()

    def add_edge(self, i, j):

        if i >= self.n:
            return
        if i == j:
            return

        self.adj[i].add(j)
        self.adj[j].add(i)

    def to_nx(self):

        nx_g = nx.Graph()

        for i in range(self.n):
            nx_g.add_node(i)

        for i in range(self.n):
            for j in self.adj[i]:
                nx_g.add_edge(i, j)

        return nx_g


class TreeDecomposition:

    def __init__(self):

        self.adj = {}  # self.adj[u] = set of neighboring bag indices
        self.bags = {}  # selg.bags[u] = list of vertices (integers)
        self.root = -1  # start unrooted

    def is_valid(self, graph):

        # all vertices ? --> init
        is_present_v = np.zeros(graph.n)

        # all edges ? --> init
        is_present_e = {}

        for i in range(graph.n):
            for j in graph.adj[i]:
                is_present_e[str((min(i, j), max(i, j)))] = False

        # loopy loop.
        for k in self.bags.keys():
            for u in self.bags[k]:
                is_present_v[u] = 1

            for u, v in combinations(self.bags[k], 2):
                try:
                    is_present_e[str((min(u, v), max(u, v)))] = True
                except KeyError:
                    # could catch ghost edges here
                    pass

        if is_present_v.sum() < graph.n:
            print("not every vertex")
            return False
        for k in is_present_e.keys():
            if not is_present_e[k]:
                print("not every edge")
                return False

        # subtree connection property --> BFS

        if self.root == -1:
            queue = [(0, -1)]
        else:
            queue = [(self.root, -1)]

        cur_extremity = {}

        started_visiting = {}
        ended_visiting = {}

        for u in range(graph.n):
            cur_extremity[u] = []
            started_visiting[u] = False
            ended_visiting[u] = False

        while len(queue) > 0:

            b, p = queue.pop(0)

            # looping over current bag:
            for u in self.bags[b]:
                # if thought we had finished with u:
                if ended_visiting[u]:
                    print("seen a vertex after ending: ", u)
                    return False

                # if seeing for first time
                if not started_visiting[u]:
                    started_visiting[u] = True
                    cur_extremity[u].append(b)
                    continue

                # if not first time seeing it
                if started_visiting[u]:

                    # if u not in parent that's wrong
                    if u not in self.bags[p]:
                        print("not in parent whereas seen before")
                        return False

                    try:
                        cur_extremity[u].remove(p)
                    except ValueError:
                        pass
                    cur_extremity[u].append(b)

            # looping over parent
            if p > -1:

                for v in self.bags[p]:
                    try:
                        cur_extremity[v].remove(p)
                    except ValueError:
                        pass

                    if len(cur_extremity[v]) == 0:
                        ended_visiting[v] = True

            # filling queue
            for new_b in self.adj[b]:
                if new_b != p:
                    queue.append((new_b, b))
                    for v in self.bags[new_b]:
                        cur_extremity[v].append(new_b)

        # if not returned at this point, valid:
        return True

    def build_from_elim_order(self, graph, order):

        # table for quick lookup
        rank = {}
        for k, u in enumerate(order):
            rank[u] = k

        # fill-in from order:
        for u in order:
            for v, w in combinations(graph.adj[u], 2):
                if (rank[v] > rank[u]) and (rank[w] > rank[u]):
                    graph.add_edge(v, w)

        # building bags
        for u in order:
            self.bags[rank[u]] = [v for v in graph.adj[u]
                                  if rank[v] > rank[u]]

            self.bags[rank[u]].append(u)

            self.adj[rank[u]] = set([])

        # connecting tree
        for u in order:
            if rank[u] < (graph.n - 1):
                ngbh = min([rank[v]
                            for v in graph.adj[u] if rank[v] > rank[u]])
            else:
                continue

            self.adj[rank[u]].add(ngbh)
            self.adj[ngbh].add(rank[u])

    def from_nx_min_fill_in(self, G):
        """
        sending the graph to nx,
        catching back the tree decomposition,
        filling self with it, without a root.
        """

        nx_G = G.to_nx()

        w, T = treewidth_min_fill_in(nx_G)

        print(T.edges)

        inv_map = {}

        for k, b in enumerate(T.nodes):

            self.bags[k] = list(b)

            self.adj[k] = set([])

            inv_map[b] = k

        for b1, b2 in T.edges:

            self.adj[inv_map[b1]].add(inv_map[b2])
            self.adj[inv_map[b2]].add(inv_map[b1])

    def pick_root(self, R=0):
        """
        Rooting the tree at bag number R.

        Rooting = give an orientation to every
        edge. parent -> child.

        Store parent separately from adj=children.
        """

        self.parent = {}
        self.root = R

        self.parent[R] = -1

        # BFS style search.
        # to_visit = "to visit next"
        to_visit = [R]

        while len(to_visit) > 0:

            u = to_visit.pop()

            for v in self.adj[u]:
                # storing parent
                self.parent[v] = u
                # only children in adj
                self.adj[v].remove(u)
                # will visit children next
                to_visit.append(v)

# IMPORTANT FUNCTIONS


def all_min_seps(graph):

    # result object
    seps = set({})

    # basis elements
    for u in range(graph.n):
        _, ngbh = components(graph.adj[u].union({u}),
                             graph.adj,
                             graph.n)

        for key in ngbh.keys():
            seps.add(frozenset(ngbh[key]))

    # rule application
    already_seen = {}

    while len(already_seen) < len(seps):

        # picking seed
        for sep in seps:
            try:
                already_seen[sep]
            except KeyError:
                seed = sep
                break

        # new seps from seed
        for u in seed:
            _, ngbh = components(graph.adj[u].union(set(seed)),
                                 graph.adj,
                                 graph.n)

            for key in ngbh.keys():
                seps.add(frozenset(ngbh[key]))

        # seed has been seen
        already_seen[seed] = True

    return seps


def sample_mspps(seps, graph, seed=None):
    """
    mspps: maximal set of pairwise parallel separators
    """

    if seed:
        random.seed(seed)

    mspps = set({})

    # add sets until not possible
    while len(seps) > 0:

        # candidate for addition into S.P.P.S
        sep = random.sample(seps, 1)[0]
        seps.remove(sep)

        # computing once for all.
        comps, _ = components(set(sep), graph.adj, graph.n)

        # does the candidate cross anything ?
        cross = False
        for sep2 in mspps:
            for k1, k2 in combinations(comps.keys(), 2):
                if k1 != k2:
                    cross |= ((not comps[k1].isdisjoint(sep2)) and (
                        not comps[k2].isdisjoint(sep2)))

        if not cross:
            mspps.add(sep)

    return mspps

# UTILITY FUNCTIONS


def plot_graph(graph,
               show=True,
               pos=None,
               node_size=50,
               node_color=[],
               savename=None):

    nx_g = nx.Graph()

    for i in range(graph.n):
        nx_g.add_node(i)

    for i in range(graph.n):
        for j in graph.adj[i]:
            nx_g.add_edge(i, j)

    if len(node_color) > 0:
        if not pos:
            nx.draw_networkx(nx_g, node_color=node_color)
        else:
            nx.draw_networkx(nx_g, node_color=node_color,
                             node_size=node_size, pos=pos)
        if savename:
            plt.savefig(savename+".eps", bbox_inches='tight')
        if show:
            plt.show()
        return
    elif pos == 'circular':
        nx.draw_circular(nx_g)
    elif not pos:
        nx.draw_networkx(nx_g)
    if show:
        plt.show()

class py_bag:

    def __init__(self, vertices):

        self.vertices = vertices
    
        self.children = []

    def add_child(self, child):

        self.children.append(child)

    def make_nice(self, child):

        self.children.remove(child)


        inter = [u for u in self.vertices if u in child.vertices]
        

        to_introduce = [v for v in self.vertices if v not in inter]

        to_forget = [v for v in child.vertices if v not in inter]

        seq = []

        cur_vertices = self.vertices

        for u in to_introduce[::-1]:

            if u == to_introduce[-1]:
                continue

            seq.append(py_bag([v for v in cur_vertices if v!= u]))

            cur_vertices = [v for v in cur_vertices if v!= u]

        if len(self.vertices) > 0:
            seq.append(py_bag(list(inter)))
        cur_vertices = inter

        for u in to_forget[::-1]:

            if u == to_forget[0]:
                continue

            cur_vertices.append(u)
            seq.append(py_bag([u for u in cur_vertices]))


    
        self.add_child(seq[0])
        for k in range(1,len(seq),1):
            seq[k-1].add_child(seq[k])

        seq[-1].add_child(child) 

    def dupli_nice(self):

        if len(self.children) > 2:

            duplicate = py_bag([u for u in self.vertices])

            for i in range(1, len(self.children),1):
                duplicate.add_child(self.children[i])

            self.children = [self.children[0]]

            self.add_child(duplicate)
            

def py_nicify(R):

    queue = [R]

    while len(queue) > 0:

        u = queue.pop()

        children = [c for c in u.children]
        for c in children:
            queue.append(c)
            u.make_nice(c)        

    queue = [R]

    while len(queue) > 0:

        u = queue.pop()

        n_children = len(u.children)

        if n_children > 2:

            for _ in range(n_children-2):
                u.dupli_nice()

        for c in u.children:
            queue.append(c)           
 
    return R
   
def py2cpp(R, tags=None):

    if not tags:
        number = 0

    queue = [R]

    cpp_equiv = {}

    while len(queue)>0:

        u = queue.pop()

        if not tags:
            cpp_u = bag(u.vertices, number)
            number += 1
        else:
            cpp_u = bag(u.vertices, tags[u])

        cpp_equiv[u] = cpp_u
    
        for c in u.children:
            queue.append(c)

    queue = [R]

    while len(queue) > 0:

        u = queue.pop()

        for c in u.children:

            cpp_equiv[u].add_child(cpp_equiv[c])

            queue.append(c)

    new_R = cpp_equiv[R]
    return cpp_equiv


def recurse_print(b, depth, tags=None):

    for _ in range(depth):
        print("   ", end="")

    if tags:
        print("tag ", tags[b], end=" ")    

    print(b, b.vertices)
    for c in b.children:
        recurse_print(c, depth+1, tags=tags)    
    


def plot_tree_dec(TD):

    g = nx.Graph()

    for u in TD.bags.keys():
        g.add_node(u)
        for v in TD.adj[u]:
            g.add_edge(u, v)

    positions = nx.spring_layout(g)

    fig, ax = plt.subplots()

    props = dict(boxstyle='round', facecolor='white', alpha=1.)

    nx.draw_networkx_edges(g, pos=positions)

    for k in TD.bags.keys():
        text = str(TD.bags[k])
        ax.text(positions[k][0], positions[k][1], text,
                bbox=props, ha='center', va='center')

    ax.set_xlim([min([positions[k][0] for k in TD.bags.keys()]),
                 max([positions[k][0] for k in TD.bags.keys()])])

    ax.set_ylim([min([positions[k][1] for k in TD.bags.keys()]),
                 max([positions[k][1] for k in TD.bags.keys()])])

    plt.axis('off')
    plt.show()

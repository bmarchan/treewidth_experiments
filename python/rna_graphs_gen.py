from graph_pylib import Graph
import random


def random_std_triple_helix(n_nucl,
                            edge_proba=0.5,
                            multibp=0.1,
                            seed=None):
    """
    as per rinaudo2012tree, a standard triple helix is what is
    called in the article a "standard structure" of degree 3.

    Not to be confused with an extended standard triple helix,
    which is not a standard structure: it is more generally
    an arc-annotated sequence allowing for an ordering wave
    embedding of degree 3
    """
    if seed:
        random.seed(seed)

    # need two new pivots
    new_pivots = random.sample(range(2, n_nucl-2), 2)

    pivots = [0, min(new_pivots), max(new_pivots), n_nucl-1]

    helix = Graph(n_nucl)

    # backbone
    for i in range(n_nucl-1):
        helix.add_edge(i, i+1)

    cur_pivot = 1

    # adding arcs
    for i in range(pivots[2]):
        if i > pivots[cur_pivot]:
            cur_pivot += 1
        if i not in pivots:
            connect_new_arc = True

            while connect_new_arc:
                connect_new_arc = False
                if random.random() < edge_proba:
                    j = random.randint(pivots[cur_pivot], pivots[cur_pivot+1])
                    helix.add_edge(i, j)
                if random.random() < multibp:
                    connect_new_arc = True

    return helix, pivots

from itertools import product
from graph_pylib import Graph, TreeDecomposition


class hashabledict(dict):
    def __hash__(self):
        return hash(tuple(sorted(self.items())))


def compatible_colorings(i, f, TD):
    """
    produces h, f_1',...,f_l' colorings
    compatible with f.

    i: index of a bag (int)

    f: dictionary. keys = colored subset of X_i.
    Usually intersection with parent bag

    TD: TreeDecomposition instance

    Returns:
        a list of lists of dictionaries.

        More precisely:

        returns l = [e1...eL]

        where each ei = [h,f_1',...f_l']

        where each h or f_j' is a dictionary.
    """

    possibilities = {}  # dict: u -> all possible color
    # tuples (h[u],f'_1(u),...,f'_l(u))

    n_children = len(TD.adj[i])

    # each vertex can be treated independently:
    for u in TD.bags[i]:

        possibilities[u] = []

        # if is colored by f
        if u in f.keys():
            # if red -> red in all children
            if f[u] == 'r':
                colors = []

                # not in h.keys():
                colors.append('NA')

                # for the children
                for j in TD.adj[i]:
                    if u in TD.bags[j]:
                        colors.append('r')
                    else:
                        colors.append('NA')

                possibilities[u] = [colors]

            # if green either green or red in children
            elif f[u] == 'g':

                # not in h.keys():
                possibilities[u] = [['NA']]

                # for the children
                for j in TD.adj[i]:
                    if u in TD.bags[j]:
                        extended_possibilities = []
                        for p in possibilities[u]:
                            pg = p.append('g')
                            pr = p.append('r')
                            extended_possibilities.append(pg)
                            extended_possibilities.append(pr)

                        possibilities[u] = extended_possibilities

                    else:
                        extended_possibilities = []
                        for p in possibilities[u]:
                            pna = p.append('NA')
                            extended_possibilities.append(pna)
                        possibilities[u] = extended_possibilities

            else:
                # orange case.

                # which children have u ?
                has_u_dict = {}
                has_u_list = []
                for j in TD.adj[i]:
                    if u in TD.bags[j]:
                        has_u_dict[j] = True
                        has_u_list.append(j)
                    else:
                        has_u_dict[j] = False

                possibilities[u] = []

                # loop over all "lucky child" possibilities.
                for lucky_child in has_u_list:
                    possibility_o = ['NA']
                    possibility_g = ['NA']
                    for j in TD.adj[i]:
                        # lucky child
                        if has_u_dict[j] and j == lucky_child:
                            possibility_o.append('o')
                            possibility_g.append('g')
                        elif has_u_dict[j] and j != lucky_child:
                            possibility_o.append('r')
                            possibility_g.append('r')
                        else:
                            possibility_o.append('NA')
                            possibility_g.append('NA')

                    possibilities[u].append(possibility_o)
                    possibilities[u].append(possibility_g)

        # u needs to be in h.keys()
        else:

            possibilities_r = []
            possibilities_g = []
            possibilities_o = []

            # red case
            colors = []

            # red in h:
            colors.append('r')

            # for the children
            for j in TD.adj[i]:
                if u in TD.bags[j]:
                    colors.append('r')
                else:
                    colors.append('NA')

            possibilities_r = [colors]

            # green case
            # green in h:
            possibilities_g = [['g']]

            # for the children
            for j in TD.adj[i]:
                if u in TD.bags[j]:
                    extended_possibilities = []
                    for p in possibilities_g:
                        pg = []
                        pg += p
                        pg.append('g')
                        pr = []
                        pr += p
                        pr.append('r')
                        extended_possibilities.append(pg)
                        extended_possibilities.append(pr)

                else:
                    extended_possibilities = []
                    for p in possibilities_g:
                        p.append('NA')
                        extended_possibilities.append(p)

                possibilities_g = extended_possibilities

            # orange case
            # which children have u ?
            has_u_dict = {}
            has_u_list = []
            for j in TD.adj[i]:
                if u in TD.bags[j]:
                    has_u_dict[j] = True
                    has_u_list.append(j)
                else:
                    has_u_dict[j] = False

            possibilities_o = []

            # loop over all "lucky child" possibilities.
            for lucky_child in has_u_list:
                possibility_o = ['o']
                possibility_g = ['o']
                for j in TD.adj[i]:
                    # lucky child
                    if has_u_dict[j] and j == lucky_child:
                        possibility_o.append('o')
                        possibility_g.append('g')
                    elif has_u_dict[j] and j != lucky_child:
                        possibility_o.append('r')
                        possibility_g.append('r')
                    else:
                        possibility_o.append('NA')
                        possibility_g.append('NA')

                possibilities_o.append(possibility_o)
                possibilities_o.append(possibility_g)

            possibilities[u] += possibilities_r
            possibilities[u] += possibilities_o
            possibilities[u] += possibilities_g

    compatible_colorings = []

    all_pos = [possibilities[u] for u in TD.bags[i]]
    for colors in product(*all_pos):

        coloring = [hashabledict() for _ in range(n_children+1)]

        for k, u in enumerate(TD.bags[i]):
            if colors[k][0] != 'NA':
                coloring[0][u] = colors[k][0]

        for j in range(n_children):
            for k, u in enumerate(TD.bags[i]):
                if colors[k][j+1] != 'NA':
                    coloring[j+1][u] = colors[k][j+1]

        compatible_colorings.append(coloring)

    return compatible_colorings


def produce_entries(TD, R=0):
    """
    Produces the list of all subproblems to compute,
    In an order that is the reverse of a topological,
    dependency-satisfying, order.

    Can be seen as a BFS of the graph of subproblems.

    Returns the list of entries, as well as a dictionary
    mapping an entry to the list of entries it needs/spawns.
    """

    entries = []

    empty_dict = hashabledict()

    new_entries = [(R, empty_dict)]

    cc_table = {}

    while len(new_entries) > 0:

        i, f = new_entries.pop()

        entries.append((i, f))

        cc_table[i, f] = compatible_colorings(i, f, TD)

        for coloring in cc_table[i, f]:

            for ind, j in enumerate(TD.adj[i]):
                new_entries.append((j, coloring[ind+1]))

    return entries, cc_table


def compute_unrealizable(F, G, TD):
    """
    Simple function computing the set of unrealizable
    edges induced by F in the graph G, with TD
    a tree decomposition of G.

    here F, is not a list of dictionaries. It is
    a dictionary, where keys are the bags of TD, and
    values are the corresponding colorings (dictionaries
    themselves)
    """

    unrealizable_set = []

    for u in G.adj.keys():
        for v in G.adj[u]:

            if u < v:  # unicity when looping

                realizable = False

                for i in TD.bags.keys():
                    if u not in TD.bags[i]:
                        continue
                    if v not in TD.bags[i]:
                        continue

                    if F[i][u] == 'g' and F[i][v] == 'g':
                        realizable = True

                if not realizable:
                    unrealizable_set.append((u, v))

    return unrealizable_set


def optimal_coloring(TD, G, w_prime):

    entries, cc_table = produce_entries(TD)

    # table
    c = {}

    # trace of subproblem realizing optimal
    trace = {}

    # taking entries in reverse order
    # compared to how produced
    for i, f in entries[::-1]:

        # will be compared to n_remove_h
        n_removed_f = 0
        for key, val in f.items():
            if val in ['o', 'r']:
                n_removed_f += 1

        # leaf case
        if len(TD.adj[i]) == 0:

            # if there is orange, +inf.
            has_orange = False

            for u in f.keys():
                if f[u] == 'o':
                    has_orange = True
                    break

            if has_orange:
                c[i, f] = 10**9
                continue

            # else, loop over possible h to find best

            cc = compatible_colorings(i, f, TD)

            best_count = 10**9
            trace[i, f] = None

            for h in cc:
                h = h[0]  # [h] -> h

                n_removed_h = 0
                for key, val in h.items():
                    if val in ['o', 'r']:
                        n_removed_h += 1

                target_size = w_prime + 1
                n_to_remove = len(TD.bags[i]) - target_size

                if n_removed_f + n_removed_h < n_to_remove:
                    continue

                unrealizable_edges = set([])

                for u in TD.bags[i]:
                    # looping over Xi\setminus D(f)
                    if u in f.keys():
                        continue
                    for v in G.adj[u]:
                        # if not in f, it is in h.keys()
                        unrealizable = False
                        if h[u] == 'r':
                            unrealizable = True
                        try:
                            if h[v] == 'r':
                                unrealizable = True
                        except KeyError:
                            pass
                        try:
                            if f[v] == 'r':
                                unrealizable = True
                        except KeyError:
                            pass

                        if unrealizable:
                            e = (min(u, v), max(u, v))  # unicity
                            unrealizable_edges.add(e)

                if len(unrealizable_edges) < best_count:
                    best_count = len(unrealizable_edges)
                    trace[i, f] = h

            c[i, f] = best_count

        # general case
        else:

            best_count = 10**9
            trace[i, f] = None

            for coloring in cc_table[i, f]:

                h = coloring[0]

                # need to remove enough to reach target
                n_removed_h = 0
                for key, val in h.items():
                    if val in ['o', 'r']:
                        n_removed_h += 1

                target_size = w_prime + 1
                n_to_remove = len(TD.bags[i]) - target_size

                # removing enough to reach target ?
                if n_removed_f + n_removed_h < n_to_remove:
                    continue

                count = 0

                # sum over children-subproblems
                for ind, j in enumerate(TD.adj[i]):
                    count += c[j, coloring[ind+1]]

                # adding del(f, h, f_{1}',...,f_{l}')

                # local objects:

                l_TD = TreeDecomposition()
                l_TD.bags[0] = TD.bags[i]

                l_TD.adj[0] = []

                for ind, j in enumerate(TD.adj[i]):
                    # little bags = Xi\capYj
                    l_TD.bags[ind+1] = list(coloring[ind+1].keys())
                    # are children of central bag
                    l_TD.adj[0].append(ind+1)
                    l_TD.adj[ind+1] = []

                shift = min(TD.bags[i])

                l_G = Graph(len(TD.bags[i]), index_shift=shift)

                for u in TD.bags[i]:
                    for v in G.adj[u]:

                        if v not in TD.bags[i]:
                            continue

                        # edge internal to D(f) ?
                        try:
                            # involving a red vertex ?
                            if f[u] == 'r' or f[v] == 'r':
                                continue  # yes -> go to next edge.

                        except KeyError:
                            pass

                        # belong to the same child ?
                        to_exclude = False

                        for ind, j in enumerate(TD.adj[i]):
                            fj = coloring[ind+1]

                            try:
                                # u orange and v o/g in Yj ?
                                if fj[u] == 'o' and (fj[v] in ['o', 'g']):
                                    to_exclude = True

                            except KeyError:
                                pass

                        if to_exclude:
                            continue

                        # if arrive to this point, edge is included
                        l_G.add_edge(u, v)

                # local coloring
                l_f = {}

                l_f0 = hashabledict()
                for key, val in f.items():
                    l_f0[key] = val
                for key, val in h.items():
                    l_f0[key] = val

                l_f[0] = l_f0

                for ind, j in enumerate(TD.adj[i]):
                    l_f[ind+1] = coloring[ind+1]

                local_del = compute_unrealizable(l_f, l_G, l_TD)

                count += len(local_del)

                if count < best_count:
                    best_count = count
                    trace[i, f] = coloring

            c[i, f] = best_count

    return c, trace


def optimal_solution(TD, c, trace, R=0):

    to_visit = [(R, hashabledict())]

    F = {}

    while len(to_visit) > 0:

        i, f = to_visit.pop()

        for key, val in f.items():
            F[i][key] = val

        coloring = trace[i, f]

        h = coloring[0]

        for key, val in h.items():
            F[i][key] = val

        for ind, j in TD.adj[i]:
            to_visit.append((j, coloring[ind+1]))

    return F

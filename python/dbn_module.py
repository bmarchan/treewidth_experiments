from graph_cpp_routines import MCF

parenthelist = ['()','[]','<>','{}','Aa','Bb','Cc','Dd']

def compute_dbn(arc_list, n_vertices):

    M = -1

    for u,v in arc_list:
        if u >= M:
            M = u
        if v >= M:
            M = v

    assert(M<= n_vertices)

    dbn = []
    for _ in range(n_vertices):
        dbn.append('.')

    snd = []
    for _ in range(n_vertices):
        snd.append('.')
    

    paren = 0

    cnt = 0

    while len(arc_list) > 0:

        mcf = MCF(arc_list, n_vertices)

        if cnt==0:
            store_mcf = mcf      
    
        cnt += 1

        print("obtained", mcf, "from ", arc_list)
        for u,v in mcf:

            if dbn[u] != '.' or dbn[v] != '.':
                print('ALERT !! MORE THAN ONE PAIR PER POSITION !!')
                continue

            dbn[u] = parenthelist[paren][0]
            dbn[v] = parenthelist[paren][1]

        paren += 1

        new_arc_list = []

        for arc in arc_list:
            if arc not in mcf:
                new_arc_list.append(arc)

        arc_list = new_arc_list

        if paren >= len(parenthelist):
            print('ALERT !! EXHAUSTED PARENTHESIS SYSTEMS')

    for u,v in store_mcf:

        snd[u] = '('
        snd[v] = ')'            

    return "".join(dbn), "".join(snd)

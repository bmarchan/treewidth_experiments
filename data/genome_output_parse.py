import os
import ast
import subprocess
import numpy as np
import sys

left_bound = 0
right_bound = 10000

base_score = { "A-A": -2.22, "C-A":  1.86, "G-A":  1.46, "U-A":  1.39,
               "A-C":  1.86, "C-C": -1.16, "G-C":  2.48, "U-C":  1.05,
               "A-G":  1.46, "C-G":  2.48, "G-G": -1.03, "U-G":  1.74,
               "A-U":  1.39, "C-U":  1.05, "G-U":  1.74, "U-U": -1.65
             }

SCORE_HEADER = "Score: "
BAG_HEADER = "Bag: "
STRUCTURE_HEADER = "Structure"
SEQUENCE_HEADER = "Sequence"
ALIGNMENT_HEADER = "Alignment"
		
#  AA           AC     AG      AU      CA     CC     CG       CU       GA     GC    GG      GU      UA      UC    UG     UU
base_pair_matrix = np.array([
[  2.49, 7.04,  8.24,  4.32,  8.84, 14.37,  4.68,  12.64,   6.86,  5.03,  8.39,  5.84,  4.01, 11.32, 6.16,  9.05], # //AA
[  7.04, 2.11,  8.89,  2.04,  9.37,  9.08,  5.86,  10.45,   9.73,  3.81, 11.05,  4.72,  5.33,  8.67, 6.93,  7.83], # //AC
[  8.24, 8.89,  0.80,  5.13, 10.41, 14.53,  4.57,  10.14,   8.61,  5.77,  5.38,  6.60,  5.43,  8.87, 5.94, 11.07], # //AG
[  4.32, 2.04,  5.13, -4.49,  5.56,  6.71, -1.67,   5.17,   5.33, -2.70,  5.61, -0.59, -1.61,  4.81, 0.51,  2.98], #  //AU
[  8.84, 9.37, 10.41,  5.56,  5.13, 10.45,  3.57,   8.49,   7.98,  5.95, 11.36,  7.93,  2.42,  7.08, 5.63,  8.39], # //CA
[ 14.37, 9.08, 14.53,  6.71, 10.45,  3.59,  5.71,   5.77,  12.43,  3.70, 12.58,  7.88,  6.88,  7.40, 8.41,  5.41], # //CC
[  4.68, 5.86,  4.57, -1.67,  3.57,  5.71, -5.36,   4.96,   6.00, -2.11,  4.66,  0.27, -2.75,  4.91,-1.32,  3.67], # //CG
[ 12.64,10.45, 10.14,  5.17,  8.49,  5.77,  4.96,   2.28,   7.71,  5.84, 13.69,  5.61,  4.72,  3.83, 7.36,  5.21], # //CU
[  6.86, 9.73,  8.61,  5.33,  7.98, 12.43,  6.00,   7.71,   1.05,  4.88,  8.67,  6.10,  5.85,  6.63, 7.55, 11.54], # //GA
[  5.03, 3.81,  5.77, -2.70,  5.95,  3.70, -2.11,   5.84,   4.88, -5.62,  4.13, -1.21, -1.60,  4.49, 0.08,  3.90], # //GC
[  8.39, 1.05,  5.38,  5.61, 11.36, 12.58,  4.66,  13.69,   8.67,  4.13,  1.98,  5.77,  5.75, 12.01, 4.27, 10.79], # //GG
[  5.84, 4.72,  6.60, -0.59,  7.93,  7.88,  0.27,   5.61,   6.10, -1.21,  5.77, -3.47,  0.57,  5.30, 2.09,  4.45], # //GU
[  4.01, 5.33,  5.43, -1.61,  2.42,  6.88, -2.75,   4.72,   5.85, -1.60,  5.75,  0.57, -4.97,  2.98,-1.14,  3.39], # //UA
[ 11.32, 8.67,  8.87,  4.81,  7.08,  7.40,  4.91,   3.83,   6.63,  4.49, 12.01,  5.30,  2.98,  3.21, 4.76,  5.97], # //UC
[  6.16, 6.93,  5.94,  0.51,  5.63,  8.41, -1.32,   7.36,   7.55,  0.08,  4.27,  2.09, -1.14,  4.76,-3.36,  4.28], #//UG
[  9.05, 7.83, 11.07,  2.98,  8.39,  5.41,  3.67,   5.21,  11.54,  3.90, 10.79,  4.45,  3.39,  5.97, 4.28,  0.02]])#//UU

bp_possibilities = ["AA","AC","AG","AU","CA","CC","CG","CU","GA","GC","GG","GU","UA","UC","UG","UU"]

base_pair_score = {}

for k, s in enumerate(bp_possibilities):
    for l, t in enumerate(bp_possibilities):
        base_pair_score[s+"-"+t] = base_pair_matrix[k,l]

id_str = sys.argv[-2]
pdb_str = sys.argv[-1]

def modify_score(query, target, excluded_edge_set, orig_score):

    involved_vertices = set([])

    #  Building set of vertices involved in excluded edge set
    for u, v in excluded_edge_set:
        involved_vertices.add(u)
        involved_vertices.add(v)

    vertex_contrib = 0

    #  Vertex contribution
    for u in involved_vertices:
        vertex_contrib -= max([base_score[query[u]+"-"+N] for N in ["A","U","G","C"]])

    edge_contrib = 0

    #  edge contribution
    for u,v in excluded_edge_set:

        edge_contrib += min([base_pair_score[query[u]+query[v]+"-"+NN] for NN in bp_possibilities])
   
    return orig_score + vertex_contrib + edge_contrib

positions = open(id_str+'/genome.position', 'r')
seq_file = open(id_str+'/seq_files/'+id_str+'.seq','r')
genome_file = open(id_str+'/'+id_str+'_ensembl.txt','r')

genome_str = ""
lines = genome_file.readlines()
for line in lines[1:]:
    genome_str += line.rstrip()

TARGET_DIR = id_str+'/target_files/'

lines_pos = positions.readlines()
seq = seq_file.readlines()[0]

incr = int(len(seq)*0.25)
lwb = int(lines_pos[0].split(' - ')[0])
upb = int(lines_pos[0].split(' - ')[1])

center_delta = 2

positions = []
indices = []

l_l = lwb-center_delta
l_r = upb+center_delta
r_l = lwb-center_delta
r_r = upb+center_delta

positions = [(l_l,l_r)]
indices = [0]
nature = ['left']


k = 0
l = 0

while (l_l >= 0 or r_r < len(genome_str)):

    l_l -= incr
    l_r -= incr
    r_l += incr
    r_r += incr

    positions = [(l_l,l_r)] + positions
    positions.append((r_l,r_r))        

    if l_l >= 0:
        k += 1
    if r_r < len(genome_str):
        l += 1

    indices = [k] + indices
    indices.append(l)

    nature = ['left'] + nature
    nature.append('right')

score = {}
positions_ok = {}
pair_ok = {}

dgf_file = open(id_str+'/dgf_files/'+pdb_str+'.dgf', 'r') 

full_edge_set = set([])

for line in dgf_file.readlines():
    if line[0]=='e':
        u = int(line.split(' ')[1][1:]) 
        v = int(line.split(' ')[2][1:]) 
        full_edge_set.add(tuple((min(u,v),max(u,v))))

ref_set = full_edge_set

best_width = 5

for width in range(best_width, 1, -1):
        
    positions_ok[width] = []

    if width < best_width:
        w_edge_set = set([])


        with open(id_str+'/td_files/w'+str(width)+'_'+id_str+'_'+pdb_str+'.log','r') as f:
            start_tracking = False
            for line in f.readlines():
                if line.find('PRESERVED') >= 0:
                    start_tracking = True
                    continue
                else:
                    if start_tracking:
                        u = int(line.split(', ')[0][1:])    
                        v = int(line.split(', ')[1].rstrip()[:-1])
                        w_edge_set.add(tuple((min(u,v),max(u,v))))    

        print("width ", width)
        print("ref set", len(ref_set))
        print("w edge set", len(w_edge_set))
        edge_set_diff = ref_set.difference(w_edge_set)

        print("diff size diff", len(edge_set_diff), edge_set_diff)

    for i in range(len(positions)):


        target_file= 'genome_'+nature[i]+'_target_'+str(indices[i])+'.seq'

        if width==best_width:
            query_file =pdb_str+'.seq'
            log_file = pdb_str\
                +'.seq_into_'+target_file+'_licorna.log'       
        else:
            query_file = 'w'+str(width)+'_'+pdb_str+'.seq'
            log_file = 'w'+str(width)+'_'+pdb_str\
                +'.seq_into_'+target_file+'_licorna.log'       

        try:
            f = open(id_str+'/licorna_logs/'+log_file,'r')
        except FileNotFoundError:
            continue        

        for line in f.readlines():

            if line.startswith('Score'):

                sc = float(line.split(': ')[1])
                break

        if width < best_width:
            with open(id_str+'/seq_files/'+query_file, 'r') as f:
                line = f.readlines()[0]
                query = line.rstrip()

            with open(id_str+'/target_files/'+target_file, 'r') as f:
                line = f.readlines()[0]                    
                target = line.rstrip()    


            score[width,i] = modify_score(query, target, edge_set_diff,sc)
           
            if positions[i][0] >= left_bound and positions[i][0] <= right_bound: 
                positions_ok[width].append(i)
        
            pair_ok[query_file+'&'+target_file] = True           

        else:
            score[width,i] = sc

            if positions[i][0] >= left_bound and positions[i][0] <= right_bound: 
                positions_ok[width].append(i)
        
            pair_ok[query_file+'&'+target_file] = True           

#    if width <= 4:
#        ref_set = w_edge_set

import json
with open(id_str+'/'+id_str+'_pair_ok.json', 'w') as f:
    json.dump(pair_ok, f)

import matplotlib.pylab as plt
import seaborn as sns
import itertools
from matplotlib.patches import Rectangle

palette = itertools.cycle(sns.color_palette())

def time_from_log(log_file):

    with open(log_file) as f:
        for line in f.readlines():
            if line.find("seconds") >= 0:
                return line[1:line.find("seconds")]

from mpl_toolkits.axes_grid.inset_locator import (inset_axes, InsetPosition,
                                                  mark_inset)
thresh = -5

#plt.rcParams['axes.grid'] = True

hit_file = open(id_str+'/'+pdb_str+'final_hits_with_thresh'+str(thresh)+'.txt', 'w')

fig, axes = plt.subplots(4, sharex=True, sharey=False, figsize=(9,4))

# Get the current reference
#ax1 = plt.gca()
#ax2 = plt.axes([0,0,1,1])
#ip = InsetPosition(ax1, [0.4,0.2,0.5,0.5])
#ax2.set_axes_locator(ip)

c = next(palette)

shift = 5000

axes[3].plot([positions[p][0]-shift for p in positions_ok[5]],
         [score[5,p] for p in positions_ok[5]], 
         color=c,
         label='full')

rectangles = []

in_rectangle = False

for p in positions_ok[5]:
    if score[5,p] <= thresh and not in_rectangle:
        cur_rec = [positions[p][0]-shift]
        in_rectangle = True
    if score[5,p] > thresh and in_rectangle:
        cur_rec.append(positions[p][0]-shift)
        in_rectangle = False
        rectangles.append(cur_rec)

if in_rectangle:
    in_rectangle = False
    rectangles.append(axes[k].get_xlim()[1])

print("rectangles", rectangles)
#axes[3].set_ylim([-80,75])
for r in rectangles:
    rect = Rectangle((r[0],axes[3].get_ylim()[0]),
                    r[1]-r[0],axes[3].get_ylim()[1]-axes[3].get_ylim()[0],
                    linewidth=1,
                    edgecolor='lightgray',
                    facecolor='lightgray'    
                    )

    axes[3].add_patch(rect)


def load_LicoRNA_alignment(p):
    lines_since_score = None
    result = []
    found_alignment = False
    for l in open(p,"r"):
        if l.startswith(SCORE_HEADER):
            sc = float(l[len(SCORE_HEADER):].strip())
            lines_since_score = 0
        elif l.startswith(STRUCTURE_HEADER):
            seq1 = l.split('\t')[1].rstrip().replace("-","")
        elif l.startswith(SEQUENCE_HEADER):
            seq2 = l.split('\t')[1].rstrip().replace("-","")
        elif l.startswith(ALIGNMENT_HEADER):
            if found_alignment:
                continue
            found_alignment=True
            align_list = l.split('\t')[1]

    new_seq1 = ''
    cnt1 = 0
    align_list = ast.literal_eval(align_list)
    for k in range(len(seq2)):
        if k in align_list:
            new_seq1 += seq1[cnt1]
            cnt1+=1
        else:
            new_seq1 += '-'

    for cnt2 in range(cnt1, len(seq1), 1):
        new_seq1 += seq1[cnt2]
    
    result.append((new_seq1,seq2,sc))
    return result    


hit_file.write('original structure sequence: \n')

with open(id_str+'/seq_files/'+pdb_str+'.seq') as f:
    for line in f.readlines():
        hit_file.write(line.rstrip()+'\n')

hit_file.write("-----"+'\n')

for r in rectangles: 
    for p in positions_ok[5]:
        if positions[p][0] - shift >= r[0] and positions[p][0]-shift < r[1]:

            target_file= 'genome_'+nature[p]+'_target_'+str(indices[p])+'.seq'
            query_file =pdb_str+'.seq'
            log_file = id_str+'/licorna_logs/'+pdb_str\
                +'.seq_into_'+target_file+'_licorna.log'       
            print("r", r, indices[p], positions[p][0], target_file)                

            print(load_LicoRNA_alignment(log_file))
            (new_seq1, seq2, sc) = load_LicoRNA_alignment(log_file)[0]
            hit_file.write(new_seq1+'\n')
            hit_file.write(seq2+'\n')
            hit_file.write("Score: "+str(sc)+'\n')
            hit_file.write("-----"+'\n')
            

hit_file.write("TIME: "+'\n')

def time_from_str(time_str):

    spl = time_str.split(':')

    mult = [1,60,3600]

    tot = 0

    for k, s in enumerate(spl[::-1]):

        tot += mult[k]*float(s)

    return tot

total_full_time = 0

num_hits = 0
for r in rectangles: 
    for p in positions_ok[5]:
        if positions[p][0] - shift >= r[0] and positions[p][0]-shift < r[1]:

            num_hits += 1
            target_file= 'genome_'+nature[p]+'_target_'+str(indices[p])+'.seq'
            query_file =pdb_str+'.seq'
            log_file = id_str+'/licorna_logs/'+pdb_str\
                +'.seq_into_'+target_file+'_licorna.log'       

            time_str = time_from_log(log_file)
            total_full_time += time_from_str(time_str)

hit_file.write("total full time: "+str(total_full_time)+" seconds\n")
hit_file.write("number hits full: "+str(num_hits)+'\n')

TOTAL_TIME = 0
for target_file in os.listdir(id_str+'/target_files/'):
    query_file =pdb_str+'.seq'
    log_file = id_str+'/licorna_logs/'+pdb_str\
        +'.seq_into_'+target_file+'_licorna.log'       

    try:      
        time_str = time_from_log(log_file)
        TOTAL_TIME += time_from_str(time_str)
    except:
        pass    

hit_file.write("total time full model no filtering: "+str(TOTAL_TIME)+'\n')

#ax2.plot([positions[p][0] for p in positions_ok[5] if positions[p][0] >= 4000 and positions[p][0] <= 6000],
#         [score[5,p] for p in positions_ok[5] if positions[p][0] >= 4000 and positions[p][0] <= 6000], 
#         color=c,
#         label='full')


f = open(id_str+'/score_data_'+id_str+'.csv', 'w')

f.write('full')

for width in range(best_width-1,2-1,-1):

    f.write(',width'+str(width))

f.write('\n')

for i in range(len(positions)):

    try:
        f.write(str(score[5,i]))
    except KeyError:
        pass

    for width in range(best_width-1,2-1,-1):

        f.write(',')
        try:
            f.write(str(score[width,i]))
        except KeyError:
            pass


    f.write('\n')

f.close()

for k, width in enumerate(range(2,best_width,1)):

    c = next(palette)

    axes[k].plot([positions[p][0]-shift for p in positions_ok[width]],
             [score[width,p] for p in positions_ok[width]],
             color=c, label='w='+str(width))

    rectangles = []

    in_rectangle = False

    for p in positions_ok[width]:
        if score[width,p] <= thresh and not in_rectangle:
            cur_rec = [positions[p][0]-shift]
            in_rectangle = True
        if score[width,p] > thresh and in_rectangle:
            cur_rec.append(positions[p][0]-shift)
            in_rectangle = False
            rectangles.append(cur_rec)

    if  in_rectangle:
        in_rectangle = False
        cur_rec.append(max([positions[p][0]-shift for p in positions_ok[width]]))
        rectangles.append(cur_rec)

    print("rectangles", rectangles)
    for r in rectangles:
        rect = Rectangle((r[0],axes[k].get_ylim()[0]),r[1]-r[0],axes[k].get_ylim()[1]-axes[k].get_ylim()[0],
                        linewidth=1,
                        edgecolor='lightgray',
                        facecolor='lightgray')
    
        axes[k].add_patch(rect)

    total_full_time = 0

    num_hits = 0
    for r in rectangles: 
        for p in positions_ok[best_width]:
            if positions[p][0] - shift >= r[0] and positions[p][0]-shift < r[1]:

                num_hits += 1
                target_file= 'genome_'+nature[p]+'_target_'+str(indices[p])+'.seq'
                query_file =pdb_str+'.seq'
                log_file = id_str+'/licorna_logs/w'+str(width)+'_'+pdb_str\
                    +'.seq_into_'+target_file+'_licorna.log'       

                time_str = time_from_log(log_file)
        
                total_full_time += time_from_str(time_str)

    hit_file.write("total width"+str(width)+" time: "+str(total_full_time)+" seconds\n")
    hit_file.write("number hits width"+str(width)+": "+str(num_hits)+'\n')
    

    
#    ax2.plot([positions[p][0] for p in positions_ok[width] if positions[p][0] >= 4000 and positions[p][0] <= 6000],
#             [score[width,p] for p in positions_ok[width] if positions[p][0] >= 4000 and positions[p][0] <= 6000],
#             color=c)

# Create a Rectangle patch
#rect = Rectangle((5000-shift,axes[3].get_ylim()[0]),50,10,linewidth=1,edgecolor='b',facecolor='b')
#
## Add the patch to the Axes
#axes[3].add_patch(rect)
#axes[3].text(5060-shift, axes[3].get_ylim()[0]+2, "true RNA position", fontsize=8)
#axes[0].text(left_bound+50, thresh+25, "threshold", color='r')
#axes[1].text(left_bound+200, thresh-15, "hits", color='g')
#axes[0].arrow(left_bound+500, thresh+20,0,-20, length_includes_head=True, color='r')
#ax = fig.add_subplot(111)

#ax.set_ylabel("Corrected alignment score")
plt.xlabel("Positions", fontsize = 14)   
fig.text(0.06, 0.5, 'Corrected alignment score (SPS)', va='center', rotation='vertical', fontsize=14) 
fig.legend(ncol=4, loc='upper center')


for k in range(4):
    axes[k].axhline(thresh,color='C5')

plt.show()

import pandas as pd

pd_score = pd.read_csv(id_str+'/score_data_'+id_str+'.csv')

g = sns.pairplot(pd_score, y_vars='full')
g.set(xlim=(-65,95), ylim=(-65,95))

for i in range(1):
    for j in range(4):
        if i!=j:
            mini = min(g.axes[i,j].get_xlim()[0], g.axes[i,j].get_ylim()[0])  
            maxi = min(g.axes[i,j].get_xlim()[1], g.axes[i,j].get_ylim()[1])  

            g.axes[i,j].plot([mini,maxi],[mini,maxi], color='r')

g.axes[0,0].axvline(thresh, color='C5')
g.axes[0,0].text(-55,65, '(a)', fontsize=16)#, fontweight='bold')
g.axes[0,1].text(-55,65, '(b)', fontsize=16)#, fontweight='bold')
g.axes[0,2].text(-55,65, '(c)', fontsize=16)#, fontweight='bold')
g.axes[0,3].text(-55,65, '(d)', fontsize=16)#, fontweight='bold')

g.savefig('scatter_plots_sps.eps')

fig.savefig('hits_and_filtering.eps', bbox_inches='tight')
plt.show()

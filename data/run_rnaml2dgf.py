import sys
import os

id_str = sys.argv[-1]

SEQ_DIR = id_str+'/seq_files/'
DGF_DIR = id_str+'/dgf_files/'

for filename in os.listdir(SEQ_DIR):

    if filename.find('.old') >= 0:
        continue

    print(filename)

    os.system('./RNAML2dgf -i '+SEQ_DIR+filename+' > '+DGF_DIR+filename[:-3]+'dgf')

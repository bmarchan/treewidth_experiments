import sys
import json

if len(sys.argv)==3:

    pdb_str = str(sys.argv[-1])
    id_str = sys.argv[-2]

else:
    pdb_str = str(sys.argv[-3])
    id_str = sys.argv[-4]
    start_width = int(sys.argv[-2]) 
    ending_width = int(sys.argv[-1])

runtime_log = open(id_str+'/simplification_rt_log_'+pdb_str+'.txt', 'w')

TD_DIR = id_str+'/td_files/'
DGF_DIR = id_str+'/dgf_files/'

best_width = 10000000

# best width ?
with open(TD_DIR+'results_'+pdb_str+'.txt','r') as f:
    for line in f.readlines():

        if int(line.split(" ")[1]) < best_width:
            best_width = int(line.split(" ")[1])

runtime_log.write('original width: '+str(best_width)+'\n')

with open(DGF_DIR+pdb_str+'.dgf','r') as f:
    line = f.readlines()[0].split(' ')

    print(line)
    runtime_log.write('original nvertices: '+line[2]+' nedges: '+line[3]+'\n')
    
if len(sys.argv)==3:
    ending_width = best_width

for target_width in range(start_width, ending_width,1):

    target_width = str(target_width)

    with open(TD_DIR+'w'+target_width+'_'+pdb_str+'_int2vertex.json','r') as f:
        int2vertex = json.load(f)
    
    # new td1.dot file    
    out_filetd = open(TD_DIR+'w'+target_width+'_'+pdb_str+'.td1.dot', 'w')

    out_filetd.write('graph G {\n') 
    out_filetd.write('\n') 

    log_file = open(TD_DIR+'w'+target_width+'_'+id_str+'_'+pdb_str+'.log')

    log_lines = log_file.readlines()

    for k, line in enumerate(log_lines):

        if line.find('BAG') == 0:

            new_line = '\t'
            new_line += 'bag'
            new_line += line.split(' ')[2]
            new_line += ' '
            new_line += '[label="'
            
            line_below = log_lines[k+2]
            l = k+2
            while line_below.find('vertex') >= 0:
                color = line_below.split(" ")[-1]
                if int(color) != 1:
                    l+=1
                    line_below=log_lines[l]
                    continue
                l += 1
                new_line+=str(int2vertex[line_below.split(" ")[1]])
                new_line +=" "
                line_below = log_lines[l]

            new_line += '"]\n'
            
            out_filetd.write(new_line)

    out_filetd.write('\n')
    
    for k, line in enumerate(log_lines):

        if line.find('BAG') >= 0:
    
            for s in line.split(" ")[5:]:
                try:
                    s = str(int(s))
                    s2 = line.split(" ")[2]

                    if int(s2) < int(s):
                        out_filetd.write('\tbag'+line.split(" ")[2]+" -- bag"+s+'\n')
                    else:
                        out_filetd.write('\tbag'+s+" -- bag"+line.split(" ")[2]+'\n')
                except:
                    continue


    out_filetd.write('\n}')

    out_filetd.close()

    # end new td1.dot file    

    # start new .seq file


    preserved_edges = []

    start_reading = False
    for line in log_lines:
        if line.find("PRESERVED") >= 0:
            start_reading = True
            continue

        if start_reading:
            e = line
        
            split = e.split(' ')

            if e.find('seconds') >= 0:
                runtime_log.write('runtime for simplifying to width '+target_width +' : '+e)
                continue 

            u = int(split[0][1:-1])
            v = int(split[1].split(')')[0])

            preserved_edges.append((int2vertex[str(u)],int2vertex[str(v)]))

    out_file_dgf= open(DGF_DIR+'w'+target_width+'_'+pdb_str+'.dgf', 'w')
            
    nvertices = len(int2vertex.items())

    nedges = len(preserved_edges)

    out_file_dgf.write('p edge '+str(nvertices)+' '+str(nedges)+'\n')

    runtime_log.write('nedges: '+str(len(preserved_edges))+' nvertices '+str(nvertices)+'\n')

    for e in preserved_edges:

        new_line = 'e '+str(e[0])+' '+str(e[1])+'\n'

        out_file_dgf.write(new_line)
        
##
##    out_file_g= open(TD_DIR+'w'+target_width+'_'+pdb_str+'.dot', 'w')
##
##    out_file_g.write('graph G {\n')
##    out_file_g.write('\n')
##    
##    with open(TD_DIR+'w'+target_width+'_'+pdb_str+'_vertex2dot_label.json','r') as f:
##        vertex2int = json.load(f)
##
##
##    for k, v in vertex2int.items():
##
##        new_line ='\t'
##        new_line += v
##        new_line += ' [label="'
##        new_line += k
##        new_line += '"]\n'
##
##        out_file_g.write(new_line)
##
##    out_file_g.write('\n')
##
##    nedges = len(log_lines[-1][1:-1].split(","))
##
##
##    start_reading = False
##
##    for line in log_lines:
##        if line.find("PRESERVED") >= 0:
##            start_reading = True
##            continue
##
##        if start_reading:
##            e = line
##        
##            print("e ", e)
##            u = e.split(', ')[0]
##            v = e.split(', ')[1][:-1]
##
##            out_file_dgf.write('e '+int2vertex[u]+' '+int2vertex[v]+'\n')
##
##            new_line = '\t'
##            new_line += vertex2int[int2vertex[u]]
##            new_line += ' -- '
##            new_line += vertex2int[int2vertex[v]]
##            new_line += '\n'
##
##            out_file_g.write(new_line)
##
##    out_file_g.write('\n}')
##    
##    out_file_dgf.close()
##    out_file_g.close()
runtime_log.close()

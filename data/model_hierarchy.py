import sys
import os

id_str = sys.argv[-2]

pdb_str = sys.argv[-1]

TD_DIR = id_str+'/td_files/'
OUT_DIR = id_str+'/td_files/'

best_width = 10000000
method = 0

with open(TD_DIR+'results_'+pdb_str+'.txt','r') as f:
    for line in f.readlines():

        if int(line.split(" ")[1]) < best_width:
            best_width = int(line.split(" ")[1])
            method = int(line.split(" ")[4])

for target_width in range(2, best_width,1):

    os.system("python3 model_diet.py "+id_str+" "+pdb_str+" "+\
                str(target_width)+" > "+OUT_DIR+"/w"+\
                str(target_width)+"_"+id_str+'_'+pdb_str+".log")

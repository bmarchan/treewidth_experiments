import sys
import os

method = sys.argv[-1]
id_str = sys.argv[-2]

DGF_DIR = '/home/bertrand/Documents/code/treewidth_experiments/data/'+id_str+'/dgf_files/'
OUT_DIR = '/home/bertrand/Documents/code/treewidth_experiments/data/'+id_str+'/td_files/'

os.chdir('/home/bertrand/Documents/code/LiCoRNA_fork/treewidth-java')

def width(td_filename):

    max_seen_width = 0

    f = open(td_filename, 'r')

    for l in f.readlines():
        if l.find('label') >= 0:

            bag_width = len(l.split('"')[1].split(' ')) - 2               

            if bag_width >= max_seen_width:
                max_seen_width = bag_width

    return str(max_seen_width)

for filename in os.listdir(DGF_DIR):

    print(filename)

    os.system('java nl.uu.cs.treewidth.TreeDecomposer '+method+' '\
                                                       +DGF_DIR+filename\
                                                       +' '+OUT_DIR+method+'_'+filename[:-3]+'td1.dot'\
                                                       +' '+OUT_DIR+method+'_'+filename[:-3]+'dot')

    f = open(OUT_DIR+'results_'+filename[:-3]+'txt','a')

    f.write('width '+width(OUT_DIR+method+'_'+filename[:-3]+'td1.dot')+' with method '+str(method)+'\n')

    f.close()


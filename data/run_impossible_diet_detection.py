from graph_classes import Bag, impossible_diet, recurse_print
import sys

target_width = int(sys.argv[-2])
td_file = open(sys.argv[-1])

td_lines = td_file.readlines()

label_adj = {}
bag2label = {}
label2bag = {}

def select_numeric(s):
    return ''.join(ch for ch in s if ch.isdigit())

max_vertices = -1

for line in td_lines:
    # identifying bag lines
    if line.find("label") >= 0:
        # building bag associated to label
        vertices = [int(select_numeric(s)) for s in line.split('=')[1].replace('\\n','').split(' ')[:-1]]
        if max(vertices) > max_vertices:
            max_vertices = max(vertices)
        label = line.split('\t')[1].split(' ')[0]
    
        label_adj[label] = []

        label2bag[label] = Bag(vertices, tag=label)

for line in td_lines:
   
    # identifying td_edges:
    if line.find("--") >= 0:
        label1 = line.split('--')[0].replace('\t','').replace(' ','')
        label2 = line.split('--')[1].replace('\n','').replace(' ','')

        label_adj[label1].append(label2)
        label_adj[label2].append(label1)

root = 'bag1'

marked = {}
for label in label_adj.keys():
    marked[label] = False

queue = [root]

while len(queue) > 0:

    label = queue.pop()

    marked[label] = True
    for ngbh_label in label_adj[label]:
        if not marked[ngbh_label]:
            label2bag[label].add_child(label2bag[ngbh_label])
            queue.append(ngbh_label)    

important_edges = [(k,k+1) for k in range(1, max_vertices,1)]

if impossible_diet(label2bag[root], target_width, important_edges):
    print("A tree-diet preserving the backbone is impossible.")
else:
    print("A tree-diet preserving the backbone is maybe possible")

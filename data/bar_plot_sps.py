import numpy as np
import sys
import os
import matplotlib.pylab as plt

id_str = sys.argv[-2]
pdb_str = sys.argv[-1]

csv_file = open(id_str+'/'+pdb_str+'_sps_log.txt', 'r')
rt_file = open(id_str+'/'+pdb_str+'_rt_log.txt', 'r')

lines = csv_file.readlines()
rt_lines = rt_file.readlines()

n_bars = len(lines[0].split(','))

results = []
rt_results = []

for k, line in enumerate(lines):

    if k == 0:  

        labels = line.split(',')

    else:

        print([repr(s) for s in line.split(',')])
        results.append([float(s.rstrip()) for s in line.split(',')]) 

for k, rt_line in enumerate(rt_lines):

    print(rt_line.split("seconds")[0][1:])

arr = np.array(results)

fig, (ax1, ax2) = plt.subplots(1,2, sharey=True)

ax1.bar(list(range(n_bars)), np.mean(arr,axis=0))

ax1.set_title(id_str)
ax1.set_xticks(list(range(n_bars)))
ax1.set_xticklabels(labels=labels)
ax1.set_ylabel('mean SPS score')

#fig.show()
ax2.set_title('run time (seconds)')
plt.show()


print(labels)
print(arr)    
print(np.mean(arr,axis=0))

import sys
import os

dgf_full = sys.argv[-2]

dgf_partial = sys.argv[-1]

draw_backbone = True

dgf_file_full = open(dgf_full, 'r')
dgf_file_partial = open(dgf_partial, 'r')


lines_full = dgf_file_full.readlines()
lines_partial = dgf_file_partial.readlines()


nvertices = int(lines_full[0].split(" ")[2]) 
print(nvertices)

tex_name = 'compare_dgf.tex'

tikz_f = open(tex_name,'w')

tikz_f.write('\\documentclass{standalone}\n')
tikz_f.write('\\usepackage{tikz}\n')
tikz_f.write('\\begin{document}\n')

tikz_f.write('\\begin{tikzpicture}\n')


for k in range(nvertices):

    tikz_f.write("\\node[circle, fill, minimum width=0.6cm] ("\
                +str(k)+") at ("+str(k)+",0) {};\n")

if draw_backbone:
    for k in range(nvertices-1):

        tikz_f.write("\\draw[thick] ("+str(k)+".east) -- ("+str(k+1)+".west);\n" )

for line in lines_full[1:]:

    print(line.split(" "))

    if line not in lines_partial:
        color = 'red'
    else:
        color='black'

    k = int(line.split(" ")[1][1:])
    l = int(line.split(" ")[2][1:])

    if k >= l:
        k, l = l, k

    if draw_backbone:
        if abs(k-l) <= 1:
            continue

    tikz_f.write("\\draw[very thick,"+str(color)+"] ("+str(l-1)+",0) arc \
                (0:180:"+str(abs(0.5*(k-l)))+");"  )

tikz_f.write('\\end{tikzpicture}\n')
tikz_f.write('\\end{document}\n')

tikz_f.close()

os.system('pdflatex '+tex_name)

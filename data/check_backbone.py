import sys

dgf_file = sys.argv[-1]

upb = -10**9
lwb = 10**9

edges = []

vertex_per_pos = {}

f = open(dgf_file, 'r')

for line in f.readlines():

    split = line.split(' ')    

    if split[0]=='e':

        edges.append(split[1]+split[2][:-1])

        pos1 = int(split[1][1:])
        pos2 = int(split[2][1:])

        vertex_per_pos[pos1] = split[1]
        vertex_per_pos[pos2] = split[2].rstrip('\n')

        if pos1 < lwb:
            lwb = pos1
        if pos2 < lwb:
            lwb = pos2
        if pos1 > upb:
            upb = pos1
        if pos2 > upb:
            upb = pos2

all_good = True

for ind in range(lwb,upb,1):

    if ind not in vertex_per_pos.keys():
        print("position ",ind," has no vertex associated to it !")
        all_good = False

    if vertex_per_pos[ind]+vertex_per_pos[ind+1] not in edges:
        all_good = False
        print("backbone edge between ", ind, " and ", ind+1," absent")

if all_good:
    print("all good")
else:
    print("not all good")
    

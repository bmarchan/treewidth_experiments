To introduce a new RFAM family into this folder, for use case #1 (RFAM multi
alignment):

    1. run python3 create_dirs.py RFAM_NAME to create the directories.
    2. run python3 copy_pdb_files.py RFAM_NAME to copy dpb_files (already all downloaded smwh else on the disk).
    3. run python3 run_dssr.py RFAM_NAME to run dssr on the pdb files.
    4. run python3 extract_graph.py RFAM_NAME 
    5. run python3 produce_seq_files.py RFAM_NAME
    6. run python3 run_rnaml2dgf.py RFAM_NAME
    7. run python3 launch_all_methods_treewidth.py RFAM_NAME
    8. run python3 model_hierarchy.py RFAM_NAME PDB_NAME
    9. run python3 parse_hierarchy_logs.py RFAM_NAME PDB_NAME 

    10.run python3 fasta_to_target.py RFAM_NAME

    11.run python3 run_licorna.py RFAM_NAME PDB_NAME 

    12.run python3 produce_afa_files.py RFAM_NAME PDB_NAME 
   
    12.run python3 licorna_log_parse.py RFAM_NAME PDB_NAME
        --> output: CSV files of run-time and SPS score

To run the tree diet algorithm and obtain a summary of exec times, numbers
of preserved edges, etc (use case #2: design examples)

    1. python3 create_dirs.py design_example has been run. dgf files 
are used as input. provided by Yann.
    2. python3 launch_all_methods_treewidth.py design_example 
    3. python3 no_mhe_model_hierarchy.py design_example ET_NAME
or 
    3. python3 no_mhe_model_hierarchy.py design_example ET_NAME STARTING_WIDTH END_WIDTH
    4. python3 dgf_parse_hierarchy_logs.py design_example ET_NAME STARTING_WIDTH END_WIDTH  
or 
    4. python3 dgf_parse_hierarchy_logs.py design_example ET_NAME
        --> output ''simplification + rt log'' with number of suppressed edges and runtime

To generate targets corresponding to sliding windows over a genomic region,
then align it with several (full and reduced models) of the same pdb. use-case #3.  
We will use some of the same scripts as use case #1, but with RFAM_NAME=PDB_NAME

    1. python3 create_dirs.py PDB_NAME
    2. genome needs no be downloaded manually, and genome.position needs to be
written. contains the actual position of the transcript in the genome file. 
    3. run python3 run_dssr.py PDB_NAME to run dssr on the pdb files.
    4. run python3 extract_graph.py PDB_NAME 
    5. run python3 produce_seq_files.py PDB_NAME
    6. run python3 run_rnaml2dgf.py PDB_NAME
    7. run python3 launch_all_methods_treewidth.py PDB_NAME
    8. run python3 model_hierarchy.py PDB_NAME PDB_NAME
    9. run python3 parse_hierarchy_logs.py PDB_NAME PDB_NAME 

    10.run python3 generate_genome_target.py PDB_NAME

    11.run python3 run_licorna.py PDB_NAME PDB_NAME 

    12.run python3 genome_output_parse.py PDB_NAME PDB_NAME 
        --> produces figures and log files 
 

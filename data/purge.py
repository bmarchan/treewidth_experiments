import os
import sys

id_str = sys.argv[-1]

os.system('rm '+id_str+'/dgf_raw_files/*.dgf')
os.system('rm '+id_str+'/dgf_files/*.dgf')
os.system('rm '+id_str+'/seq_files/*.seq')
os.system('rm '+id_str+'/licorna_logs/*.log')
os.system('rm '+id_str+'/td_files/*.dot')
os.system('rm '+id_str+'/td_files/*.json')
os.system('rm '+id_str+'/td_files/*.txt')
os.system('rm '+id_str+'/td_files/*.log')

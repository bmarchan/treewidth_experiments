import json
import os
import sys

id_str = sys.argv[-1]

DIR = './'+id_str+'/pdb_files/'
J_DIR = './'+id_str+'/json_files/'

for filename in os.listdir(DIR):
    
    if filename[:-3]+'json' in os.listdir(J_DIR):
        print("skipping ", filename)
        continue

    os.system('./x3dna-dssr --json -o='+J_DIR+filename.split('.')[0]\
                                                   +'.json -i='+DIR+filename\
                                                   +' --idstr=long')

    try:
        with open(J_DIR+filename[:-3]+'json','r') as f:
            dic = json.load(f)
    except:
        continue

import sys
import os

#if len(sys.argv)==3:
#    id_str = sys.argv[-2]
#    pdb_str = sys.argv[-1]
#
#else:
#
#    id_str = sys.argv[-3]
#    pdb_str = sys.argv[-2]
#
#    if int(sys.argv[-1]) < 1:
#        draw_backbone = False

draw_backbone = True

#DGF_DIR = id_str+'/dgf_files/'

#dgf_file = open(DGF_DIR+pdb_str+'.dgf', 'r')
dgf_name = sys.argv[-1]
dgf_file = open(sys.argv[-1], 'r')

lines = dgf_file.readlines()

print(lines[0])

nvertices = int(lines[0].split(" ")[2]) 
print(nvertices)

tikz_f = open('example.tex','w')

tikz_f.write('\\documentclass{standalone}\n')
tikz_f.write('\\usepackage{tikz}\n')
tikz_f.write('\\begin{document}\n')

tikz_f.write('\\begin{tikzpicture}\n')


for k in range(nvertices):

    tikz_f.write("\\node[circle, fill, minimum width=0.6cm] ("\
                +str(k)+") at ("+str(k)+",0) {};\n")

if draw_backbone:
    for k in range(nvertices-1):

        tikz_f.write("\\draw[thick] ("+str(k)+".east) -- ("+str(k+1)+".west);\n" )

for line in lines[1:]:

    print(line.split(" "))

    k = int(line.split(" ")[1].split('@')[1])
    l = int(line.split(" ")[2].split('@')[1])

    if k >= l:
        k, l = l, k

    if draw_backbone:
        if abs(k-l) <= 1:
            continue

    tikz_f.write("\\draw[very thick] ("+str(l-1)+",0) arc \
                (0:180:"+str(abs(0.5*(k-l)))+");"  )

tikz_f.write('\\end{tikzpicture}\n')
tikz_f.write('\\end{document}\n')

tikz_f.close()

#os.system('pdflatex '+sys.argv[-1].split('.')[0]+'.tex')
#os.system('mv '+id_str+'.* '+pdb_str+'/')

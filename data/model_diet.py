from graph_pylib import py_bag, recurse_print, py_nicify, py2cpp, Graph
from tree_diet_cpp import bag, tree_diet
import os
import sys

id_str = sys.argv[-3]
pdb_str = sys.argv[-2]
target_width = int(sys.argv[-1])

TD_DIR = id_str+'/td_files/'

td_file = open(TD_DIR+pdb_str+'.td1.dot', 'r')        
dot_file = open(TD_DIR+pdb_str+'.dot', 'r')        

vertices = set([])

td_lines = td_file.readlines()

int2vertex = {}
vertex2int = {}

lwb = 10**9
upb = -10**9

# Reading original tree decomposition: looking at vertices. 
for line in td_lines:

    line = line.replace('DU','U', 1)
    if line.find("label") >= 0:
    
        for v in line[line.find("label")+7:].split(" ")[:-1]:

            if v.find('\\n') >= 0:
                v = v[v.find('\\n')+2:]
            int2vertex[int(v[1:])] = v
            vertex2int[v] = int(v[1:])
            if int(v[1:]) > upb:
                upb = int(v[1:])
            if int(v[1:]) < lwb:
                lwb = int(v[1:])
            vertices.add(v)

adj_tree = {}

bags = set([])
bag_tag = {}

# Build and store adjacency between bags.

for line in td_lines:

    if line.find("--") >= 0:

        b1 = line.split(" -- ")[0][1:]
        b2 = line.split(" -- ")[1][:-1]

        bag_tag[b1] = int(b1[3:])
        bag_tag[b2] = int(b2[3:])    

        bags.add(b1)
        bags.add(b2)

        try:
            adj_tree[b1].append(b2)
        except KeyError:
            adj_tree[b1] = [b2]

        try: 
            adj_tree[b2].append(b1)
        except KeyError:
            adj_tree[b2] = [b1]


bag_content = {}

# Associate a bag content to each bag

for line in td_lines:

    line = line.replace('DU','U', 1)
    if line.find("label") >= 0:

        bag_label = line.split(" ")[0][1:]       

        bag_content[repr(bag_label)] = []

        for v in line[line.find("label")+7:].split(" ")[:-1]:
            if v.find('\\n') >= 0:
                v = v[v.find('\\n')+2:]
            bag_content[repr(bag_label)].append(v)

print("bag_tag", bag_tag)
print("bag_content", bag_content)
print("bags", bags)
print("TD adjacency: ", adj_tree)
print()

root =  'bag1'

queue = [root]

bag2bag = {}

pybag_tag = {}

print(vertex2int)
print(lwb,upb)

# Creating python tree decomposition through DFS exploration of tree,
# starting at root.

while len(queue) > 0:

    u = queue.pop()

    B = py_bag([vertex2int[vertex] for vertex in bag_content[repr(u)]])

    pybag_tag[B] = bag_tag[u] 
    bag2bag[u] = B

    for c in adj_tree[u]:
        if c not in bag2bag.keys():
            queue.append(c)
            adj_tree[c].remove(u)

queue = [root]

while len(queue) > 0:

    u = queue.pop()

    for c in adj_tree[u]:

        bag2bag[u].add_child(bag2bag[c])

        queue.append(c)

R = bag2bag[root]

empty_R = py_bag([])
empty_R.add_child(R)

pybag_tag[empty_R] = 0

#nice_R = py_nicify(empty_R)
nice_R = empty_R

#recurse_print(nice_R, 0)
cpp_dic = py2cpp(nice_R, tags=pybag_tag) 

cpp_R = cpp_dic[nice_R]

G = Graph(len(vertices), index_shift=lwb)

dot_lines = dot_file.readlines()

dot_label2vertex = {}
vertex2dot_label = {}

for line in dot_lines:
    line = line.replace('DU','U', 2)
    if line.find("label")>=0:
        vertex = line.split(" ")[1][8:-3]
        dot_label = line.split(" ")[0][1:]
        dot_label2vertex[dot_label] = vertex
        vertex2dot_label[vertex] = dot_label

print(vertex2dot_label)

must_have_edges = set([])

lwb = 10**9
upb = -10**9

for line in dot_lines:
    line = line.replace('DU','U', 1)
    if line.find('--') >= 0:

        dot_label1 = line.split(' -- ')[0][1:] 
        vertex1 = dot_label2vertex[dot_label1]

        dot_label2 = line.split(' -- ')[1][:-1] 
        vertex2 = dot_label2vertex[dot_label2]

        ind1 = vertex2int[vertex1]
        ind2 = vertex2int[vertex2]

        if ind1 < lwb:
            lwb = ind1
        if ind2 < lwb:
            lwb = ind2
        if ind1 > upb:
            upb = ind1
        if ind2 > upb:
            upb = ind2

        G.add_edge(vertex2int[vertex1],
                   vertex2int[vertex2])


import json

with open(TD_DIR+'w'+str(target_width)+'_'+pdb_str+'_int2vertex.json', 'w') as f:
    json.dump(int2vertex, f)

with open(TD_DIR+'w'+str(target_width)+'_'+pdb_str+'_vertex2dot_label.json', 'w') as f:
    json.dump(vertex2dot_label, f)


print("adjacency: ", G.adj)
must_have_edges = [(k,k+1) for k in range(lwb, upb, 1)] 
print("must have edges were: ", must_have_edges)
num_real, list_edges, color_dict = tree_diet(cpp_R, G.adj, target_width, list(must_have_edges))

print(num_real, len(list_edges))
print("PRESERVED EDGES: ", sorted(list_edges))
print("MISSING BACKBONE EDGES: ", sorted(list(set(must_have_edges).difference(set(list_edges)))))
print("COLOR DICT: ", color_dict)
#for e in sorted(list_edges):
#    print(e)

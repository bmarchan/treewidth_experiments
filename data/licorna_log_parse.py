import subprocess
import sys
import os

starting_width = int(sys.argv[-1])
id_str = sys.argv[-3]
pdb_str = sys.argv[-2]

LOG_DIR = id_str+'/licorna_logs/'
TD_DIR = id_str+'/td_files/'

best_width = 10000000

# best width ?
with open(TD_DIR+'results_'+pdb_str+'.txt','r') as f:
    for line in f.readlines():

        if int(line.split(" ")[1]) < best_width:
            best_width = int(line.split(" ")[1])

afa_file = id_str+'/afa_files/3_gapped'+pdb_str+'.afa'

sps_filename = id_str+'/'+pdb_str+'_sps_log.txt'
rt_filename = id_str+'/'+pdb_str+'_rt_log.txt'

sps_file = open(sps_filename, 'w')
rt_file = open(rt_filename, 'w')

if pdb_str[:3]!='w2d':
    w2d_sps_file = open(id_str+'/w2d_'+pdb_str+'_sps_log.txt')
    w2d_rt_file = open(id_str+'/w2d_'+pdb_str+'_rt_log.txt')


sps_file.write("full_model")
rt_file.write("full_model")

for width in list(range(starting_width, best_width, 1))[::-1]:

    sps_file.write(",width"+str(width))
    rt_file.write(",width"+str(width))

    if width==2 and pdb_str[:3]!='w2d':
        sps_file.write(",width2d")
        rt_file.write(",width2d")
    
rt_file.write('\n')
sps_file.write('\n')

pair_ok = {}

for target_file in os.listdir(id_str+'/target_files/'):

    if target_file.find('old') >= 0:
        continue

    log_filename = pdb_str+'.seq_into_'+target_file+'_licorna.log'

    print(target_file)
    if log_filename not in os.listdir(id_str+'/licorna_logs/'):
        print("not found")
        continue

    cmd = 'python3 analyze_alignments.py '+\
                   afa_file+' '+LOG_DIR+log_filename

    result = subprocess.run(cmd.split(' '), capture_output=True)
    print(result)
    if result.returncode > 0:
        continue

    pair_ok[pdb_str+'.seq'+'&'+target_file] = True

    sps_file.write(result.stdout.decode("utf-8").rstrip())

    log_file = open(id_str+'/licorna_logs/'+log_filename,'r')

    log_lines = log_file.readlines()

    for log_line in log_lines:
        if log_line.find("seconds") >= 0:
            rt_file.write(log_line.rstrip())

    for width in list(range(starting_width, best_width, 1))[::-1]:

        log_filename = 'w'+str(width)+'_'+pdb_str\
                +'.seq_into_'+target_file+'_licorna.log'
                 
        if log_filename not in os.listdir(id_str+'/licorna_logs/'):
            print("w not found")
            continue

        cmd =  'python3 analyze_alignments.py '+\
                    afa_file+' '+LOG_DIR+log_filename

        result = subprocess.run(cmd.split(' '), capture_output=True)
        print(result)
        if result.returncode == 0:
            pair_ok['w'+str(width)+pdb_str+'.seq'+'&'+target_file] = True

        sps_file.write(',')
        rt_file.write(',')
        sps_file.write(result.stdout.decode("utf-8").rstrip())

        if width==2 and pdb_str[:3]!='w2d':
            log_filename = 'w2d_'+pdb_str\
                    +'.seq_into_'+target_file+'_licorna.log'
                    
            if log_filename not in os.listdir(id_str+'/licorna_logs/'):
                print("w not found")
                pass
            else:

                cmd =  'python3 analyze_alignments.py '+\
                        afa_file+' '+LOG_DIR+log_filename

                result = subprocess.run(cmd.split(' '), capture_output=True)
                print(result)

                sps_file.write(',')
                rt_file.write(',')
                sps_file.write(result.stdout.decode("utf-8").rstrip())
                
        
        log_file = open(id_str+'/licorna_logs/'+log_filename,'r')

        log_lines = log_file.readlines()

        for log_line in log_lines:
            if log_line.find("seconds") >= 0:
                rt_file.write(log_line.rstrip())

                if width==2 and pdb_str[:3]!='w2d':
                    log_filename = 'w2d_'+pdb_str\
                            +'.seq_into_'+target_file+'_licorna.log'

                    log_file = open(id_str+'/licorna_logs/'+log_filename,'r')

                    log_lines = log_file.readlines()

                    for log_line in log_lines:
                        if log_line.find("seconds") >= 0:
                            rt_file.write(log_line.rstrip())
                        
                         

    sps_file.write('\n')
    rt_file.write('\n')

import json

with open(id_str+'/'+pdb_str+'_pair_ok.json','w') as f:
    json.dump(pair_ok, f)


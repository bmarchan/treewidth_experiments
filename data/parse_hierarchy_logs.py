import sys
import ast
import json

pdb_str = str(sys.argv[-1])
id_str = sys.argv[-2]


SEQ_DIR = id_str+'/seq_files/'
TD_DIR = id_str+'/td_files/'
DGF_DIR = id_str+'/dgf_files/'

best_width = 10000000

# best width ?
with open(TD_DIR+'results_'+pdb_str+'.txt','r') as f:
    for line in f.readlines():

        if int(line.split(" ")[1]) < best_width:
            best_width = int(line.split(" ")[1])

for target_width in range(2, best_width,1):

    target_width = str(target_width)

    with open(TD_DIR+'w'+target_width+'_'+pdb_str+'_int2vertex.json','r') as f:
        int2vertex = json.load(f)
    
    # new td1.dot file    
    out_filetd = open(TD_DIR+'w'+target_width+'_'+pdb_str+'.td1.dot', 'w')

    out_filetd.write('graph G {\n') 
    out_filetd.write('\n') 

    log_file = open(TD_DIR+'w'+target_width+'_'+id_str+'_'+pdb_str+'.log')

    log_lines = log_file.readlines()

    bag_lines = []

    bags_set = set([]) # So that each bag is present only once.

    color_dict_line = log_lines[-1]
    color_dict = ast.literal_eval(color_dict_line[13:])

    for bag_tag, colors in color_dict.items():
        if bag_tag not in bags_set:
            bags_set.add(bag_tag)
            new_line = '\t'
            new_line += 'bag'
            new_line += str(bag_tag) 
            new_line += ' '
            new_line += '[label="'

            for vertex, color in colors.items():
                if color==1:
                    new_line+=str(int2vertex[str(vertex)])
                    new_line +=" "

            new_line += '"]\n'
            bag_lines.append(new_line)

#    for k, line in enumerate(log_lines):
#
#        if line.find('BAG') == 0:
#
#            if line.split(' ')[2] not in bags_set:
#
#                bags_set.add(line.split(' ')[2])
#
#                new_line = '\t'
#                new_line += 'bag'
#                new_line += line.split(' ')[2]
#                new_line += ' '
#                new_line += '[label="'
#                
#                line_below = log_lines[k+2]
#                l = k+2
#                while line_below.find('vertex') >= 0:
#                    color = line_below.split(" ")[-1]
##                    print(color, int(color))
##                    print(line_below)
#                    if int(color) != 1:
#                        l+=1
#                        line_below=log_lines[l]
#                        continue
##                    print("color should be 1: ", int(color))
#                    l += 1
#                    new_line+=str(int2vertex[line_below.split(" ")[1]])
#                    new_line +=" "
#                    line_below = log_lines[l]
#
#                new_line += '"]\n'
#                
#                bag_lines.append(new_line)


    for new_line in sorted(bag_lines, key=lambda x : int(x.split('bag')[1].split(' ')[0])):
        out_filetd.write(new_line)

    out_filetd.write('\n')
    
    original_td_file = open(TD_DIR+pdb_str+'.td1.dot', 'r')

    original_lines = original_td_file.readlines()    

    for k, line in enumerate(original_lines):

        if line.find('--') >= 0:
            out_filetd.write(line)    

#            for s in line.split(" ")[5:]:
#                try:
#                    s = str(int(s))
#                    s2 = line.split(" ")[2]
#
#                    if int(s2) < int(s):
#                        out_filetd.write('\tbag'+line.split(" ")[2]+" -- bag"+s+'\n')
#                    else:
#                        out_filetd.write('\tbag'+s+" -- bag"+line.split(" ")[2]+'\n')
#                except:
#                    continue


    out_filetd.write('\n}')

    out_filetd.close()

    # end new td1.dot file    

    # start new .seq file

    out_seq_file = open(SEQ_DIR+'w'+target_width+'_'+pdb_str+'.seq', 'w')

    original_seq_file = open(SEQ_DIR+pdb_str+'.seq', 'r')

    olines = original_seq_file.readlines()

    out_seq_file.write(olines[0])


    preserved_edges = []

    arced = [] 

    preserved_edges_line = log_lines[-3]
    assert(preserved_edges_line.split(' ')[0]=='PRESERVED')
    preserved_edges = ast.literal_eval(preserved_edges_line[18:])

    for (u,v) in preserved_edges:
        if abs(u-v)==1:
            continue
        arced.append(u)
        arced.append(v)

    new_line = ''

    for k, c in enumerate(olines[1][:-1]):

        if k+1 in arced:
            new_line += c

        else:
            new_line += '.'
        
    out_seq_file.write(new_line)           

    out_seq_file.close()
            

    out_file_g= open(TD_DIR+'w'+target_width+'_'+pdb_str+'.dot', 'w')

    out_file_g.write('graph G {\n')
    out_file_g.write('\n')
    
    with open(TD_DIR+'w'+target_width+'_'+pdb_str+'_vertex2dot_label.json','r') as f:
        vertex2int = json.load(f)

    nvertices = len(vertex2int.items())

    for k, v in vertex2int.items():

        new_line ='\t'
        new_line += v
        new_line += ' [label="'
        new_line += k
        new_line += '"]\n'

        out_file_g.write(new_line)

    out_file_g.write('\n')

    nedges = len(log_lines[-1][1:-1].split(","))

    # DGF FILE
    out_file_dgf= open(DGF_DIR+'w'+target_width+'_'+pdb_str+'.dgf', 'w')
    out_file_dgf.write('p edge '+str(nvertices)+' '+str(nedges)+'\n')

    start_reading = False

    for u,v in preserved_edges:    
        out_file_dgf.write('e '+str(u)+' '+str(v)+'\n')
    out_file_dgf.close()

    # DOT FILE (GRAPH FOR LICORNA LATER) 
    for u,v in preserved_edges:

        new_line = '\t'
        new_line += vertex2int[int2vertex[str(u)]]
        new_line += ' -- '
        new_line += vertex2int[int2vertex[str(v)]]
        new_line += '\n'

        out_file_g.write(new_line)

    out_file_g.write('\n}')
    
    out_file_g.close()

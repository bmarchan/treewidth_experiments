import networkx as nx
import sys

bags_content = {}

T = nx.Graph()

f = open(sys.argv[-1],'r')

lines = f.readlines()

for l in lines:
    if l.find('label') >= 0:
        content = l.split('label=')[1]
        content = content.split(' ')

        bag_index = l.split('bag')[1].split(' ')[0]

        bags_content[bag_index] = []

        for vertex in content:
            new_vertex = ""
            for c in vertex:
                if c.isnumeric():
                    new_vertex += c

            bags_content[bag_index].append(new_vertex)

print(bags_content)

for l in lines:
    if l.find('--') >= 0:
        u = l.split('--')[0].split(' ')[0].split('bag')[1] 
        v = l.split('--')[1].split('bag')[1].rstrip('\n')
    
        T.add_edge(u,v)

layout = nx.kamada_kawai_layout(T)


tex_file = open('td_tex_file.tex','w')

tex_file.write('\\documentclass{standalone}\n')
tex_file.write('\\usepackage{tikz}\n')


tex_file.write('\\begin{document}\n')
tex_file.write('\\begin{tikzpicture}\n')

scal = 17

color_dict = {
1: 'blue!5',
2: 'blue!10',
3: 'blue!40',
4: 'orange!40',
5: 'orange!70',
6: 'orange'
}

for key, val in bags_content.items():
    label = ""
    for s in val:
        label+= " "
        label+= s
    tex_file.write('\\node[rectangle, draw, fill='+color_dict[len(val)-1]+'] ('+str(key)+') at ('+str(scal*layout[key][0])+','+str(scal*layout[key][1])+') {'+label+'};\n')

for u, v in T.edges:
    tex_file.write('\\draw[line width=1mm] ('+str(u)+') to ('+str(v)+');\n')
for key, val in bags_content.items():
    label = ""
    for s in val:
        label+= " "
        label+= s
    tex_file.write('\\node[rectangle, draw, fill='+color_dict[len(val)-1]+'] ('+str(key)+') at ('+str(scal*layout[key][0])+','+str(scal*layout[key][1])+') {'+label+'};\n')

tex_file.write('\\end{tikzpicture}\n')

import os
tex_file.write('\\end{document}\n')
tex_file.close()
os.system('pdflatex td_tex_file.tex')



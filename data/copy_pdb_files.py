import requests
import sys
import re
import os

id_str = sys.argv[-1]

r = requests.get('https://rfam.xfam.org/family/'+id_str+'/structures')

l = [m.start() for m in re.finditer('ac.uk/pdbe/entry/pdb/', r.text)]

for i in l:
    pdb_name = r.text[i+21:i+25].upper()

    if pdb_name+'.pdb' in os.listdir('/home/bertrand/Documents/code/structured_RNAs/pdb_files/'):

        os.system('cp /home/bertrand/Documents/code/structured_RNAs/pdb_files/'+\
                pdb_name+'.pdb '+id_str+'/pdb_files/')

    else:
        os.system('cp /home/bertrand/Documents/code/structured_RNAs/pdb_files/'+\
                pdb_name+'.cif '+id_str+'/pdb_files/')

    with open(id_str+'/pdb_files/'+pdb_name+'.chain','w') as f:
    
        pos = re.finditer('<td>'+pdb_name+'</td>', r.text).__next__().start()

        sub_str = r.text[pos+14:pos+35]

        chain_id = sub_str[sub_str.find('<td>')+4:sub_str.rfind('</td>')]

        sub_str2 = r.text[pos+35:pos+60]


        indices = sub_str2[sub_str2.find('<td>')+4:sub_str2.rfind('</td>')]

        f.write(chain_id+'.'+indices)

    f.close()




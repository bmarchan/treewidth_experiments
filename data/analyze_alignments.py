import ast
import os,sys

SCORE_HEADER = "Score: "
BAG_HEADER = "Bag: "
STRUCTURE_HEADER = "Structure"
SEQUENCE_HEADER = "Sequence"
ALIGNMENT_HEADER = "Alignment"

def load_LicoRNA_alignment(p):
    lines_since_score = None
    result = []
    found_alignment = False
    for l in open(p,"r"):
        if l.startswith(SCORE_HEADER):
            sc = float(l[len(SCORE_HEADER):].strip())
            lines_since_score = 0
        elif l.startswith(STRUCTURE_HEADER):
            seq1 = l.split('\t')[1].rstrip().replace("-","")
        elif l.startswith(SEQUENCE_HEADER):
            seq2 = l.split('\t')[1].rstrip().replace("-","")
        elif l.startswith(ALIGNMENT_HEADER):
            if found_alignment:
                continue
            found_alignment=True
            align_list = l.split('\t')[1]

    new_seq1 = ''
    cnt1 = 0
    align_list = ast.literal_eval(align_list)
    for k in range(len(seq2)):
        if k in align_list:
            new_seq1 += seq1[cnt1]
            cnt1+=1
        else:
            new_seq1 += '-'

    for cnt2 in range(cnt1, len(seq1), 1):
        new_seq1 += seq1[cnt2]
    
    result.append((new_seq1,seq2,sc))
    return result

def load_RFAM_gapped_FASTA(p):
    result = []
    current_seq = ""
    for l in open(p,"r"):
        if l.startswith(">"):
            if len(current_seq)>0:
                result.append(current_seq)
            current_seq = ""
        else:
            current_seq += l.strip()
    if len(current_seq)>0:
        result.append(current_seq)
    return {s.replace("-","").replace(".","").upper():s for s in result}

def get_pairwise_alignment(rfam_al,seq1,seq2):
    seq1 = seq1.replace("-","")
    seq2 = seq2.replace("-","")
    assert(seq1 in rfam_al) 
    assert(seq2 in rfam_al)
    rseq1 = rfam_al[seq1]
    rseq2 = rfam_al[seq2]
    al_pos = zip(rseq1,rseq2)
    al_pos = filter(lambda x: x!=('-','-'), al_pos)
    al1,al2 = list(zip(*al_pos))
    return "".join(al1),"".join(al2)

def SPS(cand_al,ref_al):
    def get_matched_pairs(s1,s2):
        res = []
        i,j = 0,0
        for (a,b) in zip(s1,s2):
            if a!="-" and b!="-":
                res.append((i,j))
            if a!="-":
                i += 1
            if b!="-":
                j += 1
        return set(res)          
    (cand_seq1,cand_seq2) = cand_al
    cand_pairs = get_matched_pairs(cand_seq1,cand_seq2)
    (ref_seq1,ref_seq2) = ref_al
    ref_pairs = get_matched_pairs(ref_seq1,ref_seq2)

    if len(ref_pairs)!=0:
        return len(cand_pairs & ref_pairs)/len(ref_pairs)
    else:
        return None

if __name__ == "__main__":
    lres = load_LicoRNA_alignment(sys.argv[-1]) # licorna output
    (aligned_seq1,aligned_seq2,sc) = lres[0]
    rfam_al = load_RFAM_gapped_FASTA(sys.argv[-2]) # gapped fasta from RFAM
    (ref_aligned_seq1,ref_aligned_seq2) = get_pairwise_alignment(rfam_al,aligned_seq1,aligned_seq2)
    cand_al = (aligned_seq1,aligned_seq2)
    ref_al = (ref_aligned_seq1,ref_aligned_seq2)
    print(SPS(cand_al,ref_al))

import os 
import sys

id_str = sys.argv[-2]
pdb_str = sys.argv[-1]

SEQ_DIR = id_str+'/seq_files/'
AFA_DIR = id_str+'/afa_files/'

seq_file = open(SEQ_DIR+pdb_str+'.seq','r')

fa_file = open(id_str+'/2_filtered_ungapped'+id_str+'.fa','r')

fa_file_with_pdb = open(AFA_DIR+pdb_str+'2_ungapped_seed_plus_pdb.fasta.txt','w')

for line in fa_file.readlines():

    fa_file_with_pdb.write(line)

fa_file_with_pdb.write('> PDB SEQ\n')
fa_file_with_pdb.write(seq_file.readlines()[0])

fa_file_with_pdb.close()

print("OK")

os.system('cmalign '+id_str+'/'+id_str+'.cm '+AFA_DIR+pdb_str+'2_ungapped_seed_plus_pdb.fasta.txt > \
          '+AFA_DIR+pdb_str+'.stk' )

os.system('esl-reformat -u --gapsym=- afa '+AFA_DIR+pdb_str+'.stk > '+AFA_DIR+'3_gapped'+pdb_str+'.afa')

corrupt_f = open(AFA_DIR+'3_gapped'+pdb_str+'.afa', 'r')

lines = corrupt_f.readlines()

corrupt_f.close()

corrupt_f = open(AFA_DIR+'3_gapped'+pdb_str+'.afa', 'w')

for l in lines:
    corrupt_f.write(l.replace('N','-'))

corrupt_f.close()

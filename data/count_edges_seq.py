import sys

seq_file = sys.argv[-1]

f = open(seq_file,'r')

lines = f.readlines()

dbn_line = lines[-1]

num_edges = 0

opening = ['(','[','{','<']

for c in dbn_line:
    if c in opening:
        num_edges += 1

print(num_edges+len(dbn_line)-1)

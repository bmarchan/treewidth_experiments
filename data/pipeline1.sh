#!/bin/bash

python3 create_dirs.py $1
python3 run_dssr.py $1
python3 extract_graph.py $1
python3 produce_seq_files.py $1
python3 run_rnaml2dgf.py $1
python3 launch_all_methods_treewidth.py $1

while read p; do
  python3 model_hierarchy.py $1 $p
  python3 parse_hierarchy_logs.py $1 $p
done < "$1/pdb_ids.txt"



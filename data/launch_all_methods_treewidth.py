import os
import sys

id_str = sys.argv[-1]


for i in range(1, 6, 1):
    os.system('python3 heuristic_treewidth.py '+id_str+' '+str(i))

TD_DIR = id_str+'/td_files/'

best_width = 10000000
method = 0

log_twdth = open(id_str+'_all_twdths.log','w')

log_twdth.write('pdb_str,width\n')

for dgf_file in os.listdir(id_str+'/dgf_files/'):

    if dgf_file.find('.old') >= 0:
        continue

    dgf_str = dgf_file[:-4]

    with open(TD_DIR+'results_'+dgf_str+'.txt','r') as f:
        for line in f.readlines():

            if int(line.split(" ")[1]) < best_width:
                best_width = int(line.split(" ")[1])
                method = int(line.split(" ")[4])

    log_twdth.write(dgf_str+','+str(best_width)+'\n')

    os.system('cp '+TD_DIR+str(method)+'_'+dgf_str+'.td1.dot '+TD_DIR+dgf_str+'.td1.dot')
    os.system('cp '+TD_DIR+str(method)+'_'+dgf_str+'.dot '+TD_DIR+dgf_str+'.dot')

log_twdth.close()

import sys

id_str=sys.argv[-1]

genome_file = open(id_str+'/'+id_str+'_ensembl.txt','r')

genome_str = ""

seq_file = open(id_str+'/seq_files/'+id_str+'.seq','r')

lines = genome_file.readlines()

positions = open(id_str+'/genome.position', 'r')

lines_pos = positions.readlines()

lwb = int(lines_pos[0].split(' - ')[0])
upb = int(lines_pos[0].split(' - ')[1])

for line in lines[1:]:
    genome_str += line.rstrip()

seq = seq_file.readlines()[0]

print(lwb, upb)
print(seq)
print(genome_str.replace("T","U")[lwb:upb])

incr = int(len(seq)*0.25)
print(incr)

l_targets = []
r_targets = []

center_delta = 2

l_l = lwb-center_delta
l_r = upb+center_delta
r_l = lwb-center_delta
r_r = upb+center_delta


while (l_l >= 0 or r_r < len(genome_str)):
    print("left window: ", l_l, l_r, "right window", r_l, r_r)
    try:
        l_targets.append(genome_str[l_l:l_r])
    except:
        pass
    try:
        r_targets.append(genome_str[r_l:r_r])
    except:
        pass
    l_l -= incr
    l_r -= incr
    r_l += incr
    r_r += incr

print("num left targets", len(l_targets), "num right targets ", len(r_targets))

for k, target in enumerate(l_targets):
    f = open(id_str+'/target_files/genome_left_target_'+str(k)+'.seq', 'w')
    f.write(target.replace("T","U"))
    f.close()

for k, target in enumerate(r_targets):
    f = open(id_str+'/target_files/genome_right_target_'+str(k)+'.seq', 'w')
    f.write(target.replace("T","U"))
    f.close()

import sys
import ast

file_name = sys.argv[-1]

lines = open(file_name).readlines()

# Largest bag size
content_line = lines[1]
assert(content_line.split(' ')[0]=='bag_content')
bag_content = ast.literal_eval(content_line[12:])
print(content_line)
l = [(k, v) for k, v in bag_content.items()]

for k, v in l:
    bag_content[k[1:-1]] = bag_content[k]
    bag_content.pop(k) 

largest_size = max([len(v) for _, v in bag_content.items()])
print("LARGEST BAG SIZE ", largest_size)

# IMPORTING TD ADJACENCY
td_adjacency_line = lines[3]
assert(td_adjacency_line.split(' ')[0]=='TD')

td_adjacency = ast.literal_eval(td_adjacency_line[15:])

# Largest bags: which ones exactly ? What are there neighboring separators ?
largest_bags = []

for k, v in bag_content.items():
    if len(v)==largest_size:
        largest_bags.append(k)

print("---")

print("The largest bags are: ")
for bag in largest_bags:
    print("- "+bag+" "+str(bag_content[bag]))
    print("with neighbors: "+str(td_adjacency[bag])) 
    for ngbh in td_adjacency[bag]:
        print("the separator with "+ngbh+" is ", end='')
        print(set(bag_content[bag]).intersection(set(bag_content[ngbh])))
 
# Missing edges

preserved_edges_line = lines[-3]
assert(preserved_edges_line.split(' ')[0]=='PRESERVED')
preserved_edges = ast.literal_eval(preserved_edges_line[18:])

max_vertex = max([max(u,v) for u,v in preserved_edges])

missing_edges = []

for k in range(1, max_vertex, 1):
    if (k,k+1) not in preserved_edges:
        missing_edges.append((k,k+1))

print("MISSING EDGES: ", missing_edges)

print('-----')

print('For largest bags, where are the backbone edges realized ?')

def which_sep(u, bag):

    queue = []
    marked = {}

    for ngbh in td_adjacency[bag]:
        sep = set(bag_content[bag]).intersection(set(bag_content[ngbh]))
        queue.append((ngbh, sep))
        if u in [int(v[1:]) for v in bag_content[ngbh]]:
            return sep
        marked[ngbh] = True

    while len(queue) > 0:
        cur_bag, sep = queue.pop()
        marked[cur_bag] = True
        for ngbh in td_adjacency[cur_bag]:
            try:
                marked[ngbh]
            except KeyError:
                queue.append((ngbh, sep))
                if u in [int(v[1:]) for v in bag_content[ngbh]]:
                    return sep

for bag in largest_bags:
    print("- for bag ", bag)
    for u in bag_content[bag]:
        print("-- for vertex ", u)
        index = int(u[1:])
        back_bone_neighbors = [index-1, index+1]

        for bbn in back_bone_neighbors:
            if bbn in [int(v[1:]) for v in bag_content[bag]]:
                print("ngbh ",bbn," of ",bag," is in the same bag")
            else:
                print(bbn, " is on the other side of ", which_sep(bbn, bag)) 


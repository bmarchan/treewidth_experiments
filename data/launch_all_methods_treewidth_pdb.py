import os
import sys

id_str = sys.argv[-2]
pdb_str = sys.argv[-1]


for i in range(1, 6, 1):
    os.system('python3 heuristic_treewidth.py '+id_str+' '+str(i))

TD_DIR = id_str+'/td_files/'

best_width = 10000000
method = 0

log_twdth = open(id_str+'_all_twdths.log','a')

log_twdth.write('pdb_str,width\n')

with open(TD_DIR+'results_'+pdb_str+'.txt','r') as f:
    for line in f.readlines():

        if int(line.split(" ")[1]) < best_width:
            best_width = int(line.split(" ")[1])
            method = int(line.split(" ")[4])

log_twdth.write(pdb_str+','+str(best_width)+'\n')

os.system('cp '+TD_DIR+str(method)+'_'+pdb_str+'.td1.dot '+TD_DIR+pdb_str+'.td1.dot')
os.system('cp '+TD_DIR+str(method)+'_'+pdb_str+'.dot '+TD_DIR+pdb_str+'.dot')

log_twdth.close()

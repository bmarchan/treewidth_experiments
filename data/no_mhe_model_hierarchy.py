import subprocess
import sys

if len(sys.argv)==3:
    starting_width = 3
    id_str = sys.argv[-2]
    pdb_str = sys.argv[-1]

else:

    ending_width = int(sys.argv[-1])
    starting_width = int(sys.argv[-2])
    pdb_str = sys.argv[-3]
    id_str = sys.argv[-4]

TD_DIR = id_str+'/td_files/'
OUT_DIR = id_str+'/td_files/'

best_width = 10000000
method = 0

with open(TD_DIR+'results_'+pdb_str+'.txt','r') as f:
    for line in f.readlines():

        if int(line.split(" ")[1]) < best_width:
            best_width = int(line.split(" ")[1])
            method = int(line.split(" ")[4])

if len(sys.argv)==3:
    ending_width = best_width

for target_width in range(starting_width, ending_width,1):

    print("simplifying towards width = ", target_width)

    outfile = OUT_DIR+"/w"+\
              str(target_width)+"_"+id_str+'_'+pdb_str+".log"

    cmd = "/usr/bin/time -f '%Eseconds' python3 no_mhe_model_diet.py "+id_str+" "+pdb_str+" "+\
          str(target_width)

    with open(outfile, 'w') as f:
        subprocess.call(cmd.split(' '), stdout=f, stderr=f)

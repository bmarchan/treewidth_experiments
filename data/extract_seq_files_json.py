import sys
import os
import json

id_str = sys.argv[-1]

JSON_DIR = id_str+'/json_files/'
CHAIN_DIR = id_str+'/pdb_files/'
SEQ_DIR = id_str+'/seq_files/'

for filename in os.listdir(JSON_DIR):

    with open(JSON_DIR+filename, 'r') as f:
        d = json.load(f)
    
    chain_info = open(CHAIN_DIR + filename[:-4]+'chain')

    line = chain_info.readlines()[0]

    chain_id = line.split('.')[0]
    
    lwb = int(line.split('.')[1].split(' - ')[0])
    upb = int(line.split('.')[1].split(' - ')[1])

    try:
        print(d['dbn']['chain_'+chain_id]['bseq'][lwb-1:upb-1])
        print(d['dbn']['chain_'+chain_id]['sstr'][lwb-1:upb-1])

        seq_file = open(SEQ_DIR+filename[:-4]+'seq','w')

        seq_file.write(d['dbn']['chain_'+chain_id]['bseq'][lwb-1:upb-1]+'\n')
        seq_file.write(d['dbn']['chain_'+chain_id]['sstr'][lwb-1:upb-1]+'\n')
    except KeyError:
        os.system('rm '+SEQ_DIR+filename[:-4]+'seq')    
        continue

import sys
import os
import subprocess

id_str = sys.argv[-2]
c = sys.argv[-1]
pair_ok_bool = False 

N = len(os.listdir(id_str+'/target_files/'))*len(os.listdir(id_str+'/seq_files/'))

LOG_DIR=id_str+'/licorna_logs/'

cnt = 0

import json

if pair_ok_bool:
    with open(id_str+'/'+id_str+'_pair_ok.json','r') as f:
        pair_ok = json.load(f)
else:
    pair_ok = {}
    with open(id_str+'/'+id_str+'_pair_ok.json','w') as f:
        json.dump(pair_ok,f)

for target_file in os.listdir(id_str+'/target_files/'):

    if target_file.find('.old') >= 0:
        continue

    for query_file in os.listdir(id_str+'/seq_files/'):

        if query_file.find('.old') >= 0:
            continue

        print(query_file, target_file)
        print(cnt, " out of ", N)
        cnt += 1
        with open(id_str+'/'+id_str+'_pair_ok.json','r') as f:
            pair_ok = json.load(f)
        try:
            if pair_ok[query_file+'&'+target_file]:
                print("Skipping ")
                continue
        except KeyError:
            pass

        outname = query_file+'_into_'+target_file+'_licorna.log'

        outfile = id_str+'/licorna_logs/'+query_file+'_into_'+target_file+'_licorna.log'

        cmd = "/usr/bin/time -f '%M_mrss_(kB),%U_user_time_(seconds)' ../../LiCoRNA_fork/alignerDP -i "+id_str+'/seq_files/'+query_file\
                    +' -d '+id_str+'/td_files/'+query_file[:-3]+'td1.dot'\
                    +'    -s '+id_str+'/target_files/'+target_file\
                    +' -c '+c+' -n 2' 

        print(cmd)

        with open(outfile, 'w') as f:
            f.write(cmd+'\n')
            f.write(query_file)
            f.write('\n')
            f.write(target_file)
            subprocess.call(cmd.split(' '), stdout=f, stderr=f)

        with open(id_str+'/'+id_str+'_pair_ok.json','w') as f:
            json.dump(pair_ok,f)



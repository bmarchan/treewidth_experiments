from dbn_module import compute_dbn
import sys
import os

id_str = sys.argv[-1]

DGF_DIR = '/home/bertrand/Documents/code/treewidth_experiments/data/'+id_str+'/dgf_raw_files/'
SEQ_DIR = '/home/bertrand/Documents/code/treewidth_experiments/data/'+id_str+'/seq_files/'


parenthelist = ['()','[]','<>','{}','Aa','Bb','Cc','Dd']
  
# Function to check parentheses
def check_parentheses(myStr):
    
    myStr = myStr.replace('.','')

    valid = True

    for oc in parenthelist:

        open_list = [oc[0]]
        close_list = [oc[1]]

        local_str = str([c for c in myStr if c in [oc[0],oc[1]]])

        stack = []
        for i in local_str:
            if i in open_list:
                stack.append(i)
            elif i in close_list:
                pos = close_list.index(i)
                if ((len(stack) > 0) and
                    (open_list[pos] == stack[len(stack)-1])):
                    stack.pop()
                else:
                    valid = False
        if len(stack) == 0:
            pass
        else:
            valid = False

    return valid

for filename in os.listdir(DGF_DIR):


    if filename.find('.old')>=0:
        continue

    f = open(DGF_DIR+filename, 'r')

    lines = f.readlines()

    n_vertices = int(lines[0].split(" ")[2])

    arc_list = []
    
    nucleotides = {}

    lwb = 10**9
    upb = -10**9

    for line in lines[1:]:
        
        nt1 = line.split(" ")[1].replace("DU", "U").replace("2E","").replace("5MU","U").replace("PSU","U")
        nt2 = line.split(" ")[2].replace("DU", "U").replace("2E","").replace("5MU","U").replace("PSU","U")

        ind1 = int(nt1[1:])
        ind2 = int(nt2[1:])

        if ind1 < lwb:
            lwb = ind1
        if ind2 < lwb:
            lwb = ind2
        if ind1 > upb:
            upb = ind1
        if ind2 > upb:
            upb = ind2


        nucleotides[ind1] = nt1[0]
        nucleotides[ind2] = nt2[0]

        if min(ind1, ind2) != max(ind1, ind2) - 1:

            arc_list.append((min(ind1, ind2), max(ind1, ind2))) 

    dbn_string, snd_string = compute_dbn(arc_list, upb-lwb+1)

    print("input: ",arc_list, upb-lwb+1)
    print("output :",dbn_string)
    print("output2:",snd_string)

    assert(check_parentheses(dbn_string))

    nucl_string = ''

    abort = False
    for k in range(lwb,upb+1,1):
        try:
            nucl_string += nucleotides[k]
            if nucleotides[k] == "*":
                abort = True
        except:
            nucl_string += '*'
            abort = True

    if abort:
        print("aborting ", filename)
        continue

    outfile = open(SEQ_DIR+filename[:-3]+'seq', 'w')
    outfile_snd = open(SEQ_DIR+'w2snd_'+filename[:-3]+'seq', 'w')

    outfile.write(nucl_string+'\n') 
    outfile.write(dbn_string+'\n') 
    
    outfile_snd.write(nucl_string+'\n') 
    outfile_snd.write(snd_string+'\n') 

    outfile.close()



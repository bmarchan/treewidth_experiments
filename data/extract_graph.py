import os
import json
import sys

id_str = sys.argv[-1]

DIR = id_str+'/json_files/'
CHAIN_DIR = id_str+'/pdb_files/'
OUT_DIR = id_str+'/dgf_raw_files/'


for filename in os.listdir(DIR):

    if filename=='.old':
        continue
    
    with open(DIR+filename,'r') as f:
        dic = json.load(f)

    chain_info = open(CHAIN_DIR + filename[:-4]+'chain')

    line = chain_info.readlines()[0]

    chain_id = line.split('.')[0]
    
    lwb = int(line.split('.')[1].split(' - ')[0])
    upb = int(line.split('.')[1].split(' - ')[1])

    pairs_to_keep = []   

    print(filename)

    try:
        for pair in dic['pairs']:

            chain1 = pair['nt1'].split('.')[2] 
            chain2 = pair['nt2'].split('.')[2]

            pos1 = int(pair['nt1'].split('.')[4])
            pos2 = int(pair['nt2'].split('.')[4])

            if not pair['LW'] == 'cWW':
                continue            

            if pos1 >= pos2:
                pos1, pos2 = pos2, pos1

            if chain1 == chain_id and chain2 == chain_id: 
                if pos1 >= lwb and pos1 <= upb and pos2 >= lwb and pos2 <= upb:
                    pairs_to_keep.append(pair)

    except KeyError:
        continue

    f = open(OUT_DIR+filename[:-4]+'dgf', 'w')

    n_vertices = upb - lwb + 1
    n_edges = n_vertices - 1 + len(pairs_to_keep)

    f.write('p edge '+str(n_vertices)+' '+str(n_edges)+'\n')

    nts_per_pos = {}

    for nt in dic['nts']:
        nts_per_pos[nt['index_chain'] - lwb] = nt['nt_name'] + str(nt['index_chain']-lwb)

    for pair in pairs_to_keep:

        pos1 = int(pair['nt1'].split('.')[4]) - lwb
        pos2 = int(pair['nt2'].split('.')[4]) - lwb

        nt1 = pair['nt1'].split('.')[3]+str(pos1)
        nt2 = pair['nt2'].split('.')[3]+str(pos2)

        f.write('e '+nt1+' '+nt2+'\n')

        nts_per_pos[pos1] = nt1 
        nts_per_pos[pos2] = nt2 
    
    for k in range(upb-lwb):

        try:   
            nts_per_pos[k] 
        except:
            nts_per_pos[k] = '*'+str(k)
        try:   
            nts_per_pos[k+1] 
        except:
            nts_per_pos[k+1] = '*'+str(k+1)

        f.write('e '+nts_per_pos[k]+' '+nts_per_pos[k+1]+'\n')

    f.close()
